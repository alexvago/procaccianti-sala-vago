**Escape From The Aliens In Outer Space**

Group 13 
Roberto Procaccianti
Federico Sala
Alessandro Vago

# ******INSTRUCTIONS******* #

To start a game, first launch the it.polimi.psv.server.GameManager Class, which contains the server logic.
Then launch the it.polimi.psv.main.Main class and you will be prompted to insert 0 for CLI client or 1 for GUI client

NB: if you choose GUI sometimes the first Dialog frame asking for user name doesn't popup, you can find it 
behind your other windows.

With the CLI you will have to insert the number of the action you want to take or insert the coordinates with
the format (*letter* *whitespace* *number*)
With the GUI you can choose the action on the Dialogs and click on the map when you need to choose a sector.