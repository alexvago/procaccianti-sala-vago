package it.polimi.psv.model.sector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class SectorTest {

    private Sector s1 = new Sector(0, 0);
    private Sector s2 = new Sector(0, 0);
    private GameController controller = new GameController(new GameModel(
            MapType.GALILEI));
    private Player p1 = new Human(s1, "Alex", HumanRole.ENGINEER);
    private Player p2 = new Alien(s2, "Bolg");

    private User user1 = new User();
    private User user2 = new User();

    private PlayerController humanController = new PlayerController(p1,
            controller, user1);
    private PlayerController alienController = new PlayerController(p2,
            controller, user2);

    @Test
    public final void testSector() {
        Sector s = new Sector(0, 0);
        assertNotNull(s);
    }

    @Test
    public final void testGetPlayersInSector() {
        Sector s = new Sector(0, 0);
        Set<PlayerController> set = new HashSet<PlayerController>();

        s.addPlayer(humanController);
        s.addPlayer(alienController);

        set.add(humanController);
        set.add(alienController);
        assertEquals(set, s.getPlayersInSector());

    }

    @Test
    public final void testAddPlayer() {
        Sector s = new Sector(0, 0);
        s.addPlayer(humanController);
        if (!s.getPlayersInSector().contains(humanController)) {
            fail("Il settore non contiene il Player");
        }
    }

    @Test
    public final void testGetX() {
        assertEquals(0, s1.getX());
    }

    @Test
    public final void testGetY() {
        assertEquals(0, s1.getY());
    }

    @Test
    public final void testRemovePlayer() {
        Sector s = new Sector(0, 0);
        s.addPlayer(humanController);
        s.removePlayer(humanController);
        assertFalse(s.getPlayersInSector().contains(p1));
    }

    @Test
    public final void testToString() {
        if (!(s1.toString() instanceof String)) {
            fail("not a string");
        }
    }

    @Test
    public final void testEqualsObject() {
        Sector s = new Sector(2, 2);
        assertTrue(s1.equals(s2));
        assertFalse(s1.equals(s));
    }
}
