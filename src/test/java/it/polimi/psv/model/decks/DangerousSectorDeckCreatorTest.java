package it.polimi.psv.model.decks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.DangerousSectorCard;

import org.junit.Test;

public class DangerousSectorDeckCreatorTest {

    @Test
    public void testDeckCreation() {
        int DeckSize = 25;
        Deck<DangerousSectorCard> deck = new DangerousSectorDeckCreator()
                .createDeck();
        assertNotNull(deck);
        assertEquals(DeckSize, deck.size());
    }
}
