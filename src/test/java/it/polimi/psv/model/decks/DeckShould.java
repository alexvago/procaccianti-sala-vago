package it.polimi.psv.model.decks;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.model.cards.DangerousSectorCard;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DeckShould {

    @Test
    public void reshuffleWhenEmpty() {
        Deck<DangerousSectorCard> deck = new DangerousSectorDeckCreator()
                .createDeck();
        int SIZE = deck.size();
        for (int j = 0; j < SIZE; j++) {
            DangerousSectorCard drawn = null;
            try {
                drawn = deck.draw();
            } catch (EmptyDeckException ede) {
                System.out.println("deck empty");
            }
            if (drawn != null) {
                deck.discard(drawn);
            }

        }
        assertEquals(SIZE - 1, deck.size());
    }

    @Test
    public void doNothingWhenEmpty() {
        Deck<DangerousSectorCard> deck = new DangerousSectorDeckCreator()
                .createDeck();
        int SIZE = deck.size();
        List<DangerousSectorCard> cards = new ArrayList<>();
        for (int j = 0; j < SIZE + 1; j++) {
            try {
                cards.add(deck.draw());
            } catch (EmptyDeckException ede) {
                System.out.println("deck empty");
            }
        }
        assertEquals(0, deck.size());
    }

}
