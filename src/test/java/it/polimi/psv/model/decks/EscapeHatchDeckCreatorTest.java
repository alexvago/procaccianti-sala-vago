package it.polimi.psv.model.decks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.EscapeHatchCard;

import org.junit.Test;

public class EscapeHatchDeckCreatorTest {

    @Test
    public void testDeckCreation() {
        int DeckSize = 6;
        Deck<EscapeHatchCard> deck = new EscapeHatchDeckCreator().createDeck();
        assertNotNull(deck);
        assertEquals(DeckSize, deck.size());
    }

}
