package it.polimi.psv.model.decks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.ObjectCard;

import org.junit.Test;

public class ObjectDeckCreatorTest {

    @Test
    public void testDeckCreation() {
        int objectDeckSize = 12;
        Deck<ObjectCard> deck = new ObjectDeckCreator().createDeck();
        assertNotNull(deck);
        assertEquals(objectDeckSize, deck.size());
    }

}
