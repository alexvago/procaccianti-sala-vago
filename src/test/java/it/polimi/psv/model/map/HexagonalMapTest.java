package it.polimi.psv.model.map;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.map.MapCreator;
import it.polimi.psv.model.sector.AlienSector;
import it.polimi.psv.model.sector.DangerousSector;
import it.polimi.psv.model.sector.HumanSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.SecureSector;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class HexagonalMapTest {

    File mapFile = new File("src/MapsXML/Galilei.xml");

    @Test
    public final void testHexagonalMap() {
        MapCreator.createMapFromFile(mapFile);
    }

    @Test
    public final void testGetNeighbors() {
        HexagonalMap map = MapCreator.createMapFromFile(mapFile);
        Set<Sector> expectedNeighbors = new HashSet<>();
        Set<Sector> actualNeighbors = map.getSectorsInRange(
                map.getSector(1, 1), 1);
        Sector s1 = new DangerousSector(1, 0);
        expectedNeighbors.add(s1);
        Sector s2 = new DangerousSector(2, 0);
        expectedNeighbors.add(s2);
        Sector s3 = new DangerousSector(0, 1);
        expectedNeighbors.add(s3);
        Sector s4 = new DangerousSector(2, 1);
        expectedNeighbors.add(s4);
        Sector s5 = new DangerousSector(0, 2);
        expectedNeighbors.add(s5);
        Sector s6 = new DangerousSector(1, 2);
        expectedNeighbors.add(s6);

        assertEquals(expectedNeighbors, actualNeighbors);

    }

    @Test
    public final void testGetSectorsInRange() {
        HexagonalMap map = MapCreator.createMapFromFile(mapFile);
        Set<Sector> expectedSet = new HashSet<>();
        Set<Sector> actualSet = map.getSectorsInRange(map.getSector(2, 5), 2);

        expectedSet.add(new DangerousSector(3, 3));
        expectedSet.add(new DangerousSector(2, 3));
        expectedSet.add(new DangerousSector(2, 4));
        expectedSet.add(new SecureSector(1, 4));
        expectedSet.add(new SecureSector(0, 5));
        expectedSet.add(new DangerousSector(1, 5));
        expectedSet.add(new DangerousSector(2, 6));
        expectedSet.add(new DangerousSector(1, 7));
        expectedSet.add(new DangerousSector(2, 7));
        expectedSet.add(new DangerousSector(3, 6));
        System.out.println(expectedSet + "\n" + actualSet);
        assertEquals(expectedSet, actualSet);

    }

    @Test
    public final void testGetSectorsInRangeWithCoord00() {
        HexagonalMap map = MapCreator.createMapFromFile(mapFile);
        Set<Sector> expectedSet = new HashSet<>();
        Set<Sector> actualSet = map.getSectorsInRange(map.getSector(0, 0), 1);

        expectedSet.add(new DangerousSector(0, 1));
        expectedSet.add(new DangerousSector(1, 0));
        assertEquals(expectedSet, actualSet);
    }

    @Test
    public final void testGetHumanSector() {
        HexagonalMap map = MapCreator.createMapFromFile(mapFile);
        HumanSector actualHs = map.getHumanSector();
        HumanSector expectedHs = new HumanSector(11, 2);
        assertEquals(expectedHs, actualHs);
    }

    @Test
    public final void testGetAlienSector() {
        HexagonalMap map = MapCreator.createMapFromFile(mapFile);
        AlienSector actualAs = map.getAlienSector();
        AlienSector expectedAs = new AlienSector(11, 0);
        assertEquals(expectedAs, actualAs);
    }

}
