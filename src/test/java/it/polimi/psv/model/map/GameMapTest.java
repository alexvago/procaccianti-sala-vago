package it.polimi.psv.model.map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.model.sector.Sector;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

public class GameMapTest {

    private static HashMap<Integer, Sector> map = new HashMap<Integer, Sector>();

    @BeforeClass
    public static void init() {
        for (int i = 0; i < 5; i++) {
            Sector s = new Sector(i, i);
            map.put(s.hashCode(), s);
        }

    }

    @Test
    public final void testGameMap() {
        GameMap gameMap = new HexagonalMap(map);
        assertNotNull(gameMap);
    }

    @Test
    public final void testGetSector() {
        GameMap gameMap = new HexagonalMap(map);
        gameMap.getSector(1, 1);
    }

    @Test
    public final void testIsSectorInMap() {
        GameMap gameMap = new HexagonalMap(map);
        assertTrue(gameMap.isSectorInMap(new Sector(0, 0)));
    }

}
