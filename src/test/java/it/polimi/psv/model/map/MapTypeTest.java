package it.polimi.psv.model.map;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

public class MapTypeTest {

    @Test
    public final void testGetFile() {
        File expectedFile = new File("src/MapsXML/Galilei.xml");
        File actualFile = MapType.GALILEI.getFile();
        assertEquals(expectedFile, actualFile);
    }

}
