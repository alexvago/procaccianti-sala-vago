package it.polimi.psv.model.gameModel;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.User;

import org.junit.Test;

public class gameModelTest {

    GameModel gameModel = new GameModel(MapType.GALILEI);
    private Sector sector = new Sector(0, 0);
    private HumanRole role = HumanRole.ENGINEER;
    private String name = "Asturzio";
    private String name2 = "Augias";
    private Player human = new Human(sector, name, role);
    private Player alien = new Alien(sector, name);
    private Player alien2 = new Alien(sector, name2);
    private GameController controller = new GameController(gameModel);

    private User user1 = new User();
    private User user2 = new User();
    private User user3 = new User();

    private PlayerController humanController = new PlayerController(human,
            controller, user1);
    private PlayerController alienController2 = new PlayerController(alien2,
            controller, user2);
    private PlayerController alienController = new PlayerController(alien,
            controller, user3);

    @Test
    public void testAddAndRemovePlayers() {
        gameModel.addPlayer(alienController);
        gameModel.addPlayer(humanController);

        gameModel.addPlayer(alienController2);
        assertEquals(3, gameModel.getPlayersInGame().size());
        gameModel.removePlayer(alienController2);
        assertEquals(2, gameModel.getPlayersInGame().size());
    }

    @Test
    public void testCreateDecks() {
        int sdSize = 25;
        int ehdSize = 6;
        int odSize = 12;
        assertEquals(sdSize, gameModel.getSectorDeck().size());
        assertEquals(ehdSize, gameModel.getEscapeHatchDeck().size());
        assertEquals(odSize, gameModel.getObjectDeck().size());
        System.out.println(gameModel.getSectorDeck().toString());
        try {
            gameModel.getSectorDeck().draw();
        } catch (EmptyDeckException ede) {
            System.out.println("deck empty");
        }
        assertEquals(sdSize - 1, gameModel.getSectorDeck().size());
        System.out.println(gameModel.getSectorDeck().toString());
    }

    @Test
    public void setRoundCurrentPlayerAndGameState() {
        gameModel.addPlayer(alienController);
        gameModel.addPlayer(humanController);
        gameModel.addPlayer(alienController2);
        gameModel.incrementRoundNumber();
        gameModel.incrementRoundNumber();
        gameModel.incrementRoundNumber();
        gameModel.setCurrentPlayer(human);
        assertEquals(4, gameModel.getRoundNumber());
        assertEquals(human, gameModel.getCurrentPlayer());
    }

    @Test
    public void testEndGame() {
        gameModel.addPlayer(alienController);
        gameModel.addPlayer(humanController);
        for (int rounds = 1; rounds <= 39; rounds++) {
            for (int turn = 0; turn < gameModel.getPlayersInGame().size(); turn++) {
                gameModel.incrementRoundNumber();
            }
        }
        assertEquals(GameState.GAME_END, gameModel.getGameState());
    }

}
