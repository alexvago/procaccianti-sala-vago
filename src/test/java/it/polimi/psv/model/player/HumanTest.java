package it.polimi.psv.model.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.sector.Sector;

import java.io.File;

import org.junit.Test;

public class HumanTest {

    private Sector sector = new Sector(0, 0);
    private HumanRole role = HumanRole.ENGINEER;
    private String name = "Asturzio";
    File mapFile = new File("src/MapsXML/Galilei.xml");
    private Human human = new Human(sector, name, role);

    @Test
    public void testHuman() {
        assertNotNull(human);
    }

    @Test
    public void testGetRole() {
        assertEquals(role, human.getRole());
    }

    @Test
    public void testGetPosition() {
        assertEquals(sector, human.getPosition());
    }

    @Test
    public void testAddObject() {
        ObjectCard card = new ObjectCard(ObjectCardType.ATTACK);
        human.addObject(card);
        assertTrue(human.getObjects().contains(card));
    }

    @Test
    public void testRemoveObject() {
        ObjectCard card = new ObjectCard(ObjectCardType.TELEPORT);
        ObjectCard card2 = new ObjectCard(ObjectCardType.DEFENSE);
        human.addObject(card);
        human.addObject(card2);
        ObjectCard cardRemoved = human.removeObject(new ObjectCard(
                ObjectCardType.DEFENSE));
        assertEquals(card2, cardRemoved);
        assertEquals(1, human.getObjects().size());
    }

}
