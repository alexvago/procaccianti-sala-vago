package it.polimi.psv.model.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.model.sector.Sector;

import org.junit.Test;

public class PlayerTest {
    Player captain = new Human(new Sector(0, 0), "Captain", HumanRole.CAPTAIN);
    Player pilot = new Human(new Sector(0, 0), "Pilot", HumanRole.PILOT);

    @Test
    public final void testIsUnderSedatives() {
        assertFalse(captain.isUnderSedatives());
    }

    @Test
    public final void testSetUnderSedatives() {
        captain.setUnderSedatives(true);
        assertTrue(captain.isUnderSedatives());
    }

    @Test
    public final void testGetMaxObjects() {
        assertEquals(3, captain.getMaxObjects());
    }

    @Test
    public final void testSetMaxObjects() {
        captain.setMaxObjects(4);
        assertEquals(4, captain.getMaxObjects());
    }

    @Test
    public final void testGetName() {
        assertEquals("Captain", captain.getName());
    }

    @Test
    public final void testGetPilotsNumberOfTeleports() {
        assertEquals(0, pilot.getPilotsNumberOfTeleports());
    }

    @Test
    public final void testIncrementPilotsNumberOfTeleports() {
        pilot.incrementPilotsNumberOfTeleports();
        assertEquals(1, pilot.getPilotsNumberOfTeleports());
    }

    @Test
    public final void testResetPilotsNumberOfTeleports() {
        pilot.resetPilotsNumberOfTeleports();
        assertEquals(0, pilot.getPilotsNumberOfTeleports());
    }

    @Test
    public final void testIsIdentityRevealed() {
        assertFalse(pilot.isIdentityRevealed());
    }

    @Test
    public final void testSetIdentityRevealed() {
        pilot.setIdentityRevealed(true);
        assertTrue(pilot.isIdentityRevealed());
    }

    @Test
    public final void testIsNotMoved() {
        assertFalse(pilot.isNotMoved());
    }

    @Test
    public final void testSetNotMoved() {
        pilot.setNotMoved(true);
        assertTrue(pilot.isNotMoved());
    }

    @Test
    public final void testIsCaptainPassedOnFirstDangerous() {
        assertFalse(captain.isCaptainPassedOnFirstDangerous());
    }

    @Test
    public final void testSetCaptainPassedOnFirstDangerous() {
        captain.setCaptainPassedOnFirstDangerous(true);
        assertTrue(captain.isCaptainPassedOnFirstDangerous());
    }

    @Test
    public final void testToString() {
        assertTrue(captain.toString() instanceof String);
    }

}
