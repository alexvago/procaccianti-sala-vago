package it.polimi.psv.client;

import static org.junit.Assert.assertNotNull;
import it.polimi.psv.view.logger.CLILogger;

import java.io.IOException;

import org.junit.Test;

public class ClientTest {

    @Test
    public final void testClient() throws IOException {
        assertNotNull(new Client("Alexvago", new CLILogger()));
    }

}
