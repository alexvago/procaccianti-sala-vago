package it.polimi.psv.controller.map;

import static org.junit.Assert.assertFalse;
import it.polimi.psv.model.map.HexagonalMap;
import it.polimi.psv.model.map.MapType;

import java.io.File;

import org.junit.Test;

public class MapCreatorTest {

    @Test
    public final void testCreateMapFromFile() {
        File galilei = MapType.GALILEI.getFile();
        File galvani = MapType.GALVANI.getFile();
        File fermi = MapType.FERMI.getFile();

        HexagonalMap mapGalilei = MapCreator.createMapFromFile(galilei);
        System.out.println(mapGalilei.getMap().size());
        assertFalse(mapGalilei.getMap().isEmpty());

        HexagonalMap mapGalvani = MapCreator.createMapFromFile(galvani);
        System.out.println(mapGalvani.getMap().size());
        assertFalse(mapGalvani.getMap().isEmpty());

        HexagonalMap mapFermi = MapCreator.createMapFromFile(fermi);
        System.out.println(mapFermi.getMap().size());
        assertFalse(mapFermi.getMap().isEmpty());

    }

}
