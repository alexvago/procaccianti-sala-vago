package it.polimi.psv.controller.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.player.OldSchoolPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class OldSchoolControllerTest {

    GameModel model = new GameModel(MapType.GALILEI);
    OldSchoolController osc = new OldSchoolController(model);
    Player copilot = new Human(model.getMap().getHumanSector(), "Copilot",
            HumanRole.COPILOT);
    Player psychologist = new Human(model.getMap().getHumanSector(), "Psycho",
            HumanRole.PSYCHOLOGIST);
    Player soldier = new Human(model.getMap().getHumanSector(), "Soldier",
            HumanRole.SOLDIER);
    Player engineer = new Human(model.getMap().getHumanSector(), "Engineer",
            HumanRole.ENGINEER);

    private User user1 = new User();
    private User user2 = new User();
    private User user3 = new User();
    private User user4 = new User();
    PlayerController pc1 = new OldSchoolPlayerController(copilot, osc, user1);
    PlayerController pc2 = new OldSchoolPlayerController(psychologist, osc,
            user2);
    PlayerController pc3 = new OldSchoolPlayerController(soldier, osc, user3);
    PlayerController pc4 = new OldSchoolPlayerController(engineer, osc, user4);

    @Test
    public void testInitializeCopilot() {
        osc.addPlayer(pc1);
        osc.initializeGame();
        assertEquals(1, pc1.getPlayer().getObjects().size());
        assertEquals(11, model.getObjectDeck().getGameDeck().size());
        for (ObjectCard card : model.getObjectDeck().getGameDeck()) {
            System.out.println(card.getType());
        }

    }

    @Test
    public void testInitializePsychologist() {
        osc.addPlayer(pc2);
        osc.initializeGame();
        assertEquals(model.getMap().getAlienSector(), pc2.getPlayer()
                .getPosition());
        System.out
                .println("Posizione attuale " + pc2.getPlayer().getPosition());
    }

    @Test
    public void testInitializeSoldier() {
        osc.addPlayer(pc3);
        osc.initializeGame();
        assertEquals(4, pc3.getPlayer().getMaxObjects());

    }

    /**
     * Testa le condizioni di vittoria di un player con ruolo ingegnere. Prima
     * si controlla quali sono le prime due carte nel mazzo delle carte
     * scialuppa, se ce n'è almeno una verde, la funzione deve ritornare true,
     * se ce ne sono due rosse, deve ritornare false. Poi si controlla che
     * entrambe le carte siano scartate correttamente.
     * 
     * @throws EmptyDeckException
     */
    @Test
    public void testCheckHumansWinningConditions() throws EmptyDeckException {
        osc.addPlayer(pc4);
        osc.initializeGame();
        pc4.moveToDestination(model.getMap().getSector(1, 12));

        List<EscapeHatchCard> cardsDrawn = new ArrayList<EscapeHatchCard>();
        cardsDrawn
                .add(osc.getModel().getEscapeHatchDeck().getGameDeck().get(0));
        cardsDrawn
                .add(osc.getModel().getEscapeHatchDeck().getGameDeck().get(1));

        for (EscapeHatchCard card : cardsDrawn) {
            System.out.println(card.type());
        }

        if (cardsDrawn.contains(new EscapeHatchCard(EscapeHatchType.GREEN))) {
            assertTrue(osc.checkHumansWinningConditions(pc4));
            System.out.println("Almeno una verde");
        } else {
            assertFalse(osc.checkHumansWinningConditions(pc4));
            System.out.println("Entrambe rosse");
        }

        assertEquals(2, model.getEscapeHatchDeck().getGraveyard().size());
    }
}