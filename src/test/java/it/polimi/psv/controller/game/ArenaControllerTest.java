package it.polimi.psv.controller.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.player.ArenaPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import org.junit.Test;

public class ArenaControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private ArenaController ac = new ArenaController(model);
    private Player alien = new Alien(model.getMap().getHumanSector(), "Alien");
    private User user = new User();
    private User user2 = new User();
    private PlayerController pc1 = new ArenaPlayerController(alien, ac, user,
            model.getMap().getSector(1, 12));
    private PlayerController pc2 = new ArenaPlayerController(alien, ac, user2,
            model.getMap().getSector(1, 12));

    @Test
    public void testCheckDestination() {
        ac.addPlayer(pc1);
        ac.initializeGame();
        assertFalse(ac.checkDestination(pc1.getPlayer(), model.getMap()
                .getSector(0, 12)));
        assertTrue(ac.checkDestination(pc1.getPlayer(), model.getMap()
                .getSector(1, 11)));
        pc1.moveToDestination(model.getMap().getSector(2, 9));
        assertTrue(ac.checkDestination(pc1.getPlayer(), model.getMap()
                .getSector(4, 7)));
    }

    @Test
    public void testCheckAlienWinningConditions() {
        ac.addPlayer(pc1);
        ac.addPlayer(pc2);
        pc1.attack();
        pc1.attack();
        assertTrue(ac.checkAliensWinningConditions());
        assertEquals(1, model.getWinners().size());
    }
}
