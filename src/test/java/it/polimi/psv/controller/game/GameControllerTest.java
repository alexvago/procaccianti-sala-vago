package it.polimi.psv.controller.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.User;
import it.polimi.psv.server.publisher.Publisher;

import org.junit.Test;

public class GameControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameControllerThread tr = new GameControllerThread(MapType.GALILEI);
    private GameController controller = new GameController(model,
            new Publisher(), tr);

    private User user1 = new User();
    private User user2 = new User();
    private User user3 = new User();

    private PlayerController pc1 = new PlayerController(new Human(model
            .getMap().getHumanSector(), "Umano", HumanRole.COPILOT),
            controller, user1);
    private PlayerController pc2 = new PlayerController(new Alien(model
            .getMap().getAlienSector(), "Alieno1"), controller, user2);
    private PlayerController pc3 = new PlayerController(new Human(model
            .getMap().getHumanSector(), "Umano2", HumanRole.COPILOT),
            controller, user3);

    @Test
    public void testCheckAliensWinningConditions() {

        controller.addPlayer(pc1);
        controller.addPlayer(pc2);
        controller.initializeGame();
        assertFalse(controller.checkAliensWinningConditions());
        System.out.println(model.getPlayersInGame().size());
        pc1.moveToDestination(model.getMap().getSector(5, 0));
        pc2.moveToDestination(model.getMap().getSector(5, 0));
        pc2.attack();
        System.out.println(model.getPlayersInGame().size());
        assertTrue(controller.checkAliensWinningConditions());
    }

    @Test
    public void testCheckHumanWinningConditions() throws EmptyDeckException {
        controller.addPlayer(pc1);
        controller.addPlayer(pc3);

        pc1.moveToDestination(model.getMap().getEscapeHatces().get(0));
        model.getEscapeHatchDeck().getGameDeck()
                .set(0, new EscapeHatchCard(EscapeHatchType.GREEN));
        assertTrue(controller.checkHumansWinningConditions(pc1));
        assertEquals(EscapeHatchType.RED,
                model.getMap().getEscapeHatces().get(0).getType());
        System.out.println("First EscapeHatch type: "
                + model.getMap().getEscapeHatces().get(0).getType());

        pc3.moveToDestination(model.getMap().getEscapeHatces().get(1));
        model.getEscapeHatchDeck().getGameDeck()
                .set(0, new EscapeHatchCard(EscapeHatchType.RED));
        assertFalse(controller.checkHumansWinningConditions(pc3));
        assertEquals(EscapeHatchType.RED,
                model.getMap().getEscapeHatces().get(1).getType());
        System.out.println("Second EscapeHatch type: "
                + model.getMap().getEscapeHatces().get(1).getType());

        assertEquals(1, model.getPlayersInGame().size());
    }

    @Test
    public void testRemoveDisconnectedPlayer() {
        controller.addPlayer(pc1);
        controller.addPlayer(pc2);
        controller.removeDisconnectedPlayer(pc1);
        assertEquals(1, model.getPlayersInGame().size());
    }
}
