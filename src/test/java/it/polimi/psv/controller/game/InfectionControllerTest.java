package it.polimi.psv.controller.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.player.InfectionPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.InfectionControllerThread;
import it.polimi.psv.server.User;
import it.polimi.psv.server.publisher.Publisher;

import org.junit.Test;

public class InfectionControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameControllerThread tr = new InfectionControllerThread(
            MapType.GALILEI);
    private GameController ic = new InfectionController(model, new Publisher(),
            tr);

    private User user1 = new User();
    private User user2 = new User();

    private PlayerController pc1 = new InfectionPlayerController(new Human(
            model.getMap().getHumanSector(), "Umano", HumanRole.COPILOT), ic,
            user1);
    private PlayerController pc2 = new InfectionPlayerController(new Human(
            model.getMap().getHumanSector(), "Umano2", HumanRole.ENGINEER), ic,
            user2);

    @Test
    public final void testInitializeGame() {
        ic.initializeGame();
        for (Sector s : model.getMap().getMap().values()) {
            if (s instanceof EscapeHatchSector) {
                System.out.println(s + " " + ((EscapeHatchSector) s).getType());
                assertEquals(EscapeHatchType.GREEN,
                        ((EscapeHatchSector) s).getType());
            }
        }
    }

    @Test
    public void testCheckHumanWinningCondition() throws EmptyDeckException {
        ic.addPlayer(pc1);
        ic.addPlayer(pc2);
        ic.initializeGame();
        pc1.moveToDestination(model.getMap().getEscapeHatces().get(0));
        assertTrue(ic.checkHumansWinningConditions(pc1));
        pc2.moveToDestination(model.getMap().getEscapeHatces().get(0));
        assertFalse(ic.checkHumansWinningConditions(pc2));
    }

}
