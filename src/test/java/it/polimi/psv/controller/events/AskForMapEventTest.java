package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.model.map.MapType;

import org.junit.Test;

public class AskForMapEventTest {

    MapType[] maps = MapType.values();
    AskForMapEvent event = new AskForMapEvent(maps);

    @Test
    public final void testGetMaps() {
        MapType[] maps = event.getMaps();
        assertEquals(3, maps.length);

    }

}
