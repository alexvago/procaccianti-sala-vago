package it.polimi.psv.controller.events;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class AskForPositionEventTest {

    private String message = "test";
    private ArrayList<int[]> possibleSectors = new ArrayList<int[]>();

    private AskForPositionEvent event = new AskForPositionEvent(message,
            possibleSectors);

    @Test
    public final void testAskForPositionEvent() {
        assertNotNull(event);
    }

    @Test
    public final void testGetMessage() {
        possibleSectors.add(new int[] { 0, 0 });
        System.out.println(event.getMessage());
        assertTrue(event.getMessage().matches("test\n.*"));
    }

}
