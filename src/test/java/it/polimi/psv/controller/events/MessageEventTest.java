package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class MessageEventTest {

    String message = "TestMessage";
    MessageEvent event = new MessageEvent(message);

    @Test
    public final void testMessageEvent() {
        assertNotNull(event);
    }

    @Test
    public final void testGetMessage() {
        assertEquals(message, event.getMessage());
    }

}
