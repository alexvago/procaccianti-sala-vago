package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.controller.useractions.UserAction;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SendPossibleActionsEventTest {
    private List<UserAction> actions = new ArrayList<UserAction>();

    @Test
    public final void testSendPossibleActionsEvent() {
        SendPossibleActionsEvent event = new SendPossibleActionsEvent(actions);
        assertNotNull(event);
    }

    @Test
    public final void testGetPossibleActions() {
        actions.add(UserAction.ATTACK);
        SendPossibleActionsEvent event = new SendPossibleActionsEvent(actions);
        assertEquals(1, event.getPossibleActions().size());

    }

}
