package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.game.RuleType;

import org.junit.Test;

public class AskForRulesEventTest {

    AskForRulesEvent event = new AskForRulesEvent();

    @Test
    public final void testGetRules() {
        RuleType[] rules = event.getRules();
        assertEquals(4, rules.length);
    }

}
