package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.ObjectCardType;

import org.junit.Test;

public class AskToDiscardEventTest {
    ObjectCardType[] types = ObjectCardType.values();
    int typesSize = types.length;
    AskToDiscardEvent event = new AskToDiscardEvent(types);

    @Test
    public final void testAskToDiscardEvent() {
        assertNotNull(event);

    }

    @Test
    public final void testGetTypes() {
        assertEquals(typesSize, event.getTypes().length);
    }

}
