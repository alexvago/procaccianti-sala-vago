package it.polimi.psv.controller.events;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class NoiseInYourSectorEventTest {

    NoiseInYourSectorEvent event = new NoiseInYourSectorEvent();

    @Test
    public final void testGetMessage() {
        assertNotNull(event.getMessage());
    }

}
