package it.polimi.psv.controller.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.ObjectCardType;

import org.junit.Test;

public class AskForCardToUseEventTest {

    private ObjectCardType[] types = new ObjectCardType[] { ObjectCardType.ADRENALINE };
    private AskForCardToUseEvent event = new AskForCardToUseEvent(types);

    @Test
    public final void testAskForCardToUseEvent() {
        assertNotNull(event);
    }

    @Test
    public final void testGetTypes() {
        assertEquals(1, event.getTypes().length);
    }

}
