package it.polimi.psv.controller.decks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.state.GameModel;

import org.junit.Test;

public class DeckControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);

    private DeckController deckController = new DeckController(model);

    @Test
    public void testDeckController() {
        assertNotNull(deckController);
    }

    @Test
    public void testDrawAndDiscardObjectCard() throws EmptyDeckException {
        ObjectCard card = deckController.drawObjectCard();
        assertEquals(11, model.getObjectDeck().getGameDeck().size());
        deckController.discardObjectCard(card);
        assertEquals(1, model.getObjectDeck().getGraveyard().size());
    }

    @Test
    public void testDrawAndDiscardSectorCard() throws EmptyDeckException {
        DangerousSectorCard card = deckController.drawSectorCard();
        assertEquals(24, model.getSectorDeck().getGameDeck().size());
        deckController.discardSectorCard(card);
        assertEquals(1, model.getSectorDeck().getGraveyard().size());
    }

    @Test
    public void testDrawAndDiscardEscapeHatchCard() throws EmptyDeckException {
        EscapeHatchCard card = deckController.drawEscapeHatchCard();
        assertEquals(5, model.getEscapeHatchDeck().getGameDeck().size());
        deckController.discardEscapeHatchCard(card);
        assertEquals(1, model.getEscapeHatchDeck().getGraveyard().size());
    }

}
