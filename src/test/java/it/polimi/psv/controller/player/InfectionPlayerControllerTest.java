package it.polimi.psv.controller.player;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.game.InfectionController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;
import it.polimi.psv.server.publisher.Publisher;

import org.junit.Test;

public class InfectionPlayerControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new InfectionController(model,
            new Publisher(), null);
    private Player p = new Human(model.getMap().getHumanSector(), "Alex",
            HumanRole.PSYCHOLOGIST);
    private User user1 = new User();
    private User user2 = new User();
    private User user3 = new User();
    private InfectionPlayerController ipc = new InfectionPlayerController(p,
            controller, user1);

    @Test
    public final void testInfectionPlayerController() {
        assertNotNull(ipc);
    }

    @Test
    public final void testAttack() {
        Player p2 = new Alien(model.getMap().getHumanSector(), "Alien1");
        InfectionPlayerController ipc2 = new InfectionPlayerController(p2,
                controller, user2);
        Player p3 = new Human(model.getMap().getHumanSector(), "Umano",
                HumanRole.CAPTAIN);
        InfectionPlayerController ipc3 = new InfectionPlayerController(p3,
                controller, user3);
        ipc3.getPlayer().addObject(new ObjectCard(ObjectCardType.DEFENSE));

        System.out.println(p);
        ipc2.attack();
        System.out.println(ipc.getPlayer());
        System.out.println(ipc.getPlayer().getPosition().getPlayersInSector());
        assertTrue(ipc.getPlayer() instanceof Alien);
        assertTrue(p3 instanceof Human);
        assertTrue(ipc.getInfected());
    }

}
