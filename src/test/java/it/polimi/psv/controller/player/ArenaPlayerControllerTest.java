package it.polimi.psv.controller.player;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.game.ArenaController;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import org.junit.Test;

public class ArenaPlayerControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new ArenaController(model);

    private User user1 = new User();
    private User user2 = new User();

    private Player p = new Alien(model.getMap().getHumanSector(), "Alex");
    private Sector initialPosition = model.getMap().getSector(2, 4);
    private PlayerController pc = new ArenaPlayerController(p, controller,
            user1, initialPosition);

    private Player p2 = new Alien(model.getMap().getHumanSector(), "Pippo");
    private Sector initialPosition2 = model.getMap().getSector(2, 4);
    private PlayerController pc2 = new ArenaPlayerController(p2, controller,
            user2, initialPosition2);

    private Sector destination = model.getMap().getSector(1, 4);

    @Test
    public final void testAttack() {
        pc2.movePlayer(destination);
        pc.movePlayer(destination);
        pc.attack();
        assertEquals(initialPosition2, pc2.getPlayer().getPosition());
    }

}
