package it.polimi.psv.controller.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.User;
import it.polimi.psv.server.publisher.Publisher;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class PlayerControllerTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameControllerThread tr = new GameControllerThread(MapType.GALILEI);
    private GameController controller = new GameController(model,
            new Publisher(), tr);

    private Player player = new Human(model.getMap().getHumanSector(), "John",
            HumanRole.CAPTAIN);
    private User user = new User();
    private PlayerController pCont = new PlayerController(player, controller,
            user);

    @Test
    public final void testPlayerController() {
        assertNotNull(pCont);
    }

    @Test
    public final void testMovePlayer() {
        List<Sector> sectors = new ArrayList<>(model.getMap()
                .getSectorsInRange(player.getPosition(), player.getSpeed()));
        assertTrue(pCont.movePlayer(sectors.get(0)));
        int x = 11, y = 0;
        Sector destination = model.getMap().getSector(x, y);
        System.out.println(destination);
        assertFalse(pCont.movePlayer(destination));
    }

    @Test
    public final void testAttack() {

        PlayerController p1 = new PlayerController(new Human(model.getMap()
                .getHumanSector(), "asd", HumanRole.COPILOT), controller, user);

        p1.getPlayer().addObject(new ObjectCard(ObjectCardType.DEFENSE));
        PlayerController p2 = new PlayerController(new Alien(model.getMap()
                .getHumanSector(), "asd"), controller, user);
        System.out.println(p2.getPlayer().getPosition().getPlayersInSector());
        p2.attack();
        assertEquals(2, p2.getPlayer().getPosition().getPlayersInSector()
                .size());
        assertEquals(3, p2.getPlayer().getSpeed());
    }

    @Test
    public final void testSetPlayer() {
        PlayerController p1 = new PlayerController(new Human(model.getMap()
                .getHumanSector(), "asd", HumanRole.COPILOT), controller, user);
        System.out.println(p1.getPlayer());
        p1.setPlayer(new Alien(p1.getPlayer().getPosition(), p1.getPlayer()
                .getName()));
        System.out.println(p1.getPlayer());
    }

    @Test
    public final void testGetPossibleDestinations() {
        PlayerController p1 = new PlayerController(new Human(model.getMap()
                .getSector(10, 0), "asd", HumanRole.COPILOT), controller, user);
        Set<Sector> expectedSectors = new HashSet<>();

        expectedSectors.add(model.getMap().getSector(10, -1));
        expectedSectors.add(model.getMap().getSector(11, -1));
        expectedSectors.add(model.getMap().getSector(9, 0));
        expectedSectors.add(model.getMap().getSector(9, 1));

        assertEquals(expectedSectors, p1.getPossibleDestinations());
    }

    @Test
    public final void testHasPlayableCards() {
        PlayerController p1 = new PlayerController(new Human(model.getMap()
                .getSector(10, 0), "asd", HumanRole.COPILOT), controller, user);
        assertFalse(p1.hasPlayableCards());
        p1.getPlayer().addObject(new ObjectCard(ObjectCardType.SEDATIVES));
        assertTrue(p1.hasPlayableCards());
    }

}
