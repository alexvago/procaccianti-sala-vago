package it.polimi.psv.controller.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.User;

import org.junit.Test;

public class SedativesActionTest {

    private Sector sector = new Sector(1, 1);
    private HumanRole role = HumanRole.ENGINEER;
    private String name = "Asturzio";
    private Player human = new Human(sector, name, role);
    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new GameController(model);
    private User user = new User();

    private PlayerController pc = new PlayerController(human, controller, user);

    private Action action = new SedativesAction();

    @Test
    public void testExecuteAction() {
        ObjectCard card = new ObjectCard(ObjectCardType.SEDATIVES);
        human.addObject(card);
        action.executeAction(pc);
        assertTrue(human.isUnderSedatives());
        model.setGameState(GameState.ENDTURN);
        assertFalse(human.isUnderSedatives());

        /**
         * tests if the SedativesAction deletes the observer after the reset of
         * the boolean "underSedatives" called in the update method.
         */
        model.setGameState(GameState.AFTER_MOVE);
        human.setUnderSedatives(true);
        model.setGameState(GameState.ENDTURN);
        assertTrue(human.isUnderSedatives());

        /**
         * tests if the card is removed from the player's list and discarded
         * correctly.
         */
        assertEquals(1, model.getObjectDeck().getGraveyard().size());
        assertEquals(0, human.getObjects().size());
    }

}
