package it.polimi.psv.controller.actions;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.game.OldSchoolController;
import it.polimi.psv.controller.player.OldSchoolPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import org.junit.Test;

public class TeleportActionTest {

    private Sector sector = new Sector(1, 1);
    private HumanRole role = HumanRole.ENGINEER;
    private String name = "Asturzio";
    private Player human = new Human(sector, name, role);
    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new GameController(model);
    private Player pilot = new Human(sector, "pilot", HumanRole.PILOT);
    private GameController osc = new OldSchoolController(model);
    private User user1 = new User();
    private User user2 = new User();

    private PlayerController pc = new PlayerController(human, controller, user1);
    private PlayerController ospc = new OldSchoolPlayerController(pilot, osc,
            user2);

    private Action action = new TeleportAction();

    @Test
    public void testTeleport() {
        ObjectCard card = new ObjectCard(ObjectCardType.TELEPORT);
        human.addObject(card);
        action.executeAction(pc);
        assertEquals(model.getMap().getHumanSector(), human.getPosition());

        /**
         * tests if the card is removed from the player's list and discarded
         * correctly.
         */
        assertEquals(1, model.getObjectDeck().getGraveyard().size());
        assertEquals(0, human.getObjects().size());

    }

    @Test
    public void testPilotTeleportInOldSchool() {
        ObjectCard card = new ObjectCard(ObjectCardType.TELEPORT);
        pilot.addObject(card);
        action.executeAction(ospc);
        assertEquals(model.getMap().getHumanSector(), pilot.getPosition());
        assertEquals(1, ospc.getPlayer().getPilotsNumberOfTeleports());
        action.executeAction(ospc);
        action.executeAction(ospc);
        /**
         * Testa se il numero di teletrasporti usati dal pilota viene resettato
         * a 0
         */
        assertEquals(0, ospc.getPlayer().getPilotsNumberOfTeleports());
        /**
         * Testa se la carta viene scartata correttamente
         */
        assertEquals(1, model.getObjectDeck().getGraveyard().size());
        assertEquals(0, human.getObjects().size());
    }

}
