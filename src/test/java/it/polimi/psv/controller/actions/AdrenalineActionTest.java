package it.polimi.psv.controller.actions;

import static org.junit.Assert.assertEquals;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.User;

import org.junit.Test;

public class AdrenalineActionTest {

    private Sector sector = new Sector(1, 1);
    private HumanRole role = HumanRole.ENGINEER;
    private String name = "Asturzio";
    private Player human = new Human(sector, name, role);
    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new GameController(model);
    private User user = new User();

    private PlayerController pc = new PlayerController(human, controller, user);

    private Action action = new AdrenalineAction();

    @Test
    public void adrenalineTest() {
        ObjectCard card = new ObjectCard(ObjectCardType.ADRENALINE);
        human.addObject(card);
        action.executeAction(pc);
        assertEquals(2, human.getSpeed());
        model.setGameState(GameState.AFTER_ATTACK);
        model.setGameState(GameState.ENDTURN);
        assertEquals(1, human.getSpeed());

        /**
         * tests if the adrenalineAction deletes the observer after the speed
         * decrement called in the update method.
         */
        model.setGameState(GameState.AFTER_MOVE);
        human.incrementSpeed();
        model.setGameState(GameState.ENDTURN);
        assertEquals(2, human.getSpeed());

        /**
         * tests if the card is removed from the player's list and discarded
         * correctly.
         */
        assertEquals(1, model.getObjectDeck().getGraveyard().size());
        assertEquals(0, human.getObjects().size());
    }

}
