package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.User;

import org.junit.Test;

public class SpotlightActionTest {

    private GameModel model = new GameModel(MapType.GALILEI);
    private GameController controller = new GameController(model);
    private Player human = new Human(model.getMap().getHumanSector(),
            "Asturzio", HumanRole.ENGINEER);
    private Player human2 = new Human(model.getMap().getHumanSector(), "Fede",
            HumanRole.CAPTAIN);
    private Player alien = new Alien(model.getMap().getAlienSector(), "Ale");
    private User user1 = new User();
    private User user2 = new User();
    private User user3 = new User();

    private PlayerController humanController = new PlayerController(human,
            controller, user1);
    private PlayerController humanController2 = new PlayerController(human2,
            controller, user2);
    private PlayerController alienController = new PlayerController(alien,
            controller, user3);

    private Action action = new SpotlightAction();

    @Test
    public void SpotlightTest() {

        // ricordarsi che l'aggiunta al settore deve avvenire in automatico!!!
        human.setPosition(model.getMap().getSector(1, 1));
        model.getMap().getSector(1, 1).addPlayer(humanController);
        human2.setPosition(model.getMap().getSector(1, 0));
        model.getMap().getSector(1, 0).addPlayer(humanController2);
        alien.setPosition(model.getMap().getSector(2, 1));
        model.getMap().getSector(2, 1).addPlayer(alienController);
        System.out.println(human.getPosition().toString());
        System.out.println(human2.getPosition().toString());
        System.out.println(alien.getPosition().toString());

        ((SpotlightAction) action).setCoordinates(2, 1);
        action.executeAction(humanController);

    }

}
