package it.polimi.psv.controller.decks;

import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.state.GameModel;

/**
 * Controller of decks, contains methods to draw/discard cards from all the
 * decks.
 */

public class DeckController {

    private final GameModel model;

    /**
     * Constructor of a Deck Controller.
     *
     * @param model
     *            The Game model of the current game.
     */

    public DeckController(GameModel model) {
        this.model = model;
    }

    /**
     * draws one object card from the ObjectDeck
     * 
     * @return the card drawn
     * @throws EmptyDeckException
     */
    public ObjectCard drawObjectCard() throws EmptyDeckException {
        return model.getObjectDeck().draw();
    }

    /**
     * draws one sector card from the SectorDeck
     * 
     * @return the card drawn
     * @throws EmptyDeckException
     */
    public DangerousSectorCard drawSectorCard() throws EmptyDeckException {
        return model.getSectorDeck().draw();
    }

    /**
     * draws one escape hatch card from the EscapeHatchDeck
     * 
     * @return the card drawn
     * @throws EmptyDeckException
     */
    public EscapeHatchCard drawEscapeHatchCard() throws EmptyDeckException {
        return model.getEscapeHatchDeck().draw();
    }

    /**
     * discards the given card in ObjectDeck graveyard
     * 
     * @param card
     */
    public void discardObjectCard(ObjectCard card) {
        model.getObjectDeck().discard(card);
    }

    /**
     * discards sector card
     * 
     * @param card
     */
    public void discardSectorCard(DangerousSectorCard card) {
        model.getSectorDeck().discard(card);
    }

    /**
     * discards escapeHatchCard card
     * 
     * @param card
     */
    public void discardEscapeHatchCard(EscapeHatchCard card) {
        model.getEscapeHatchDeck().discard(card);
    }

}
