package it.polimi.psv.controller.map;

import it.polimi.psv.model.map.HexagonalMap;
import it.polimi.psv.model.sector.AlienSector;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.HumanSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.view.logger.CLILogger;
import it.polimi.psv.view.logger.Logger;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Creator of Maps, starting from a XML file with a list of Sectors
 *
 */

public class MapCreator {

    private static final Logger LOGGER = new CLILogger();

    private MapCreator() {
    }

    /**
     * Create a map starting from a XML file
     *
     * @param file
     *            XML file containing the map
     * @return return a GameMap object initialized with the chosen map
     *
     */
    public static HexagonalMap createMapFromFile(File file) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Map<Integer, Sector> map = new HashMap<Integer, Sector>();
        HumanSector hs = null;
        AlienSector as = null;
        List<EscapeHatchSector> escapeHatces = new ArrayList<EscapeHatchSector>();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document parsedMap = builder.parse(file);

            NodeList columnsList = parsedMap.getElementsByTagName("column");
            for (int i = 0; i < columnsList.getLength(); i++) {

                Element column = (Element) columnsList.item(i);

                NodeList sectorsList = column.getElementsByTagName("sector");
                for (int j = 0; j < sectorsList.getLength(); j++) {
                    Element sectorElement = (Element) sectorsList.item(j);
                    String sectorType = sectorElement
                            .getElementsByTagName("type").item(0)
                            .getTextContent();

                    int y = j - (i / 2);
                    Sector sector = createSectorFromString(sectorType, i, y);
                    if (sector instanceof AlienSector) {
                        as = (AlienSector) sector;
                    }
                    if (sector instanceof HumanSector) {
                        hs = (HumanSector) sector;
                    }
                    if (sector instanceof EscapeHatchSector) {
                        escapeHatces.add((EscapeHatchSector) sector);
                    }
                    map.put(sector.hashCode(), sector);
                }
            }
        } catch (ClassNotFoundException cnfe) {

            LOGGER.print("Can't find a Sector class, check the XML file of the map for errors");
            LOGGER.print(cnfe);

        } catch (Exception e) {
            LOGGER.print("Cannot instanciate the map, exiting.." + "\n");
            LOGGER.print(e);
        }
        return new HexagonalMap(map, hs, as, escapeHatces);
    }

    /**
     * Private method for creating Sector objects out of XML node
     * 
     * @param string
     *            The specific Sector class name
     * @param i
     *            the column coordinate
     * @param y
     *            the axial coordinate
     * @return Sector with specific runtime type
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    private static Sector createSectorFromString(String string, int i, int y)
            throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException,
            IllegalAccessException {

        String sectorsPackage = "it.polimi.psv.model.sector.";
        Class<?> c = Class.forName(sectorsPackage + string);
        Constructor<?> cons = c.getConstructor(int.class, int.class);
        return (Sector) cons.newInstance(i, y);

    }

}
