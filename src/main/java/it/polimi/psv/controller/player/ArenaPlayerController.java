package it.polimi.psv.controller.player;

import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.server.User;

/**
 * Player Controller for Arena.
 */
public class ArenaPlayerController extends PlayerController {

    private int score = 0;
    private final Sector initialPosition;

    /**
     * Constructor of an Arena Player Controller.
     *
     * @param player
     *            the player
     * @param controller
     *            Controller of the Game
     * @param initialPosition
     *            Initial Position of the Player.
     */

    public ArenaPlayerController(Player player, GameController controller,
            User user, Sector initialPosition) {

        super(player, controller, user);
        this.initialPosition = initialPosition;
        moveToDestination(initialPosition);
    }

    /**
     * Override of Attack Routine, in arena when a Player is killed restart from
     * the initial Position, and the killer receive one point, added to his
     * score.
     *
     * @param file
     */

    @Override
    protected void attackRoutine(PlayerController p) {
        if (p.getPlayer() instanceof Alien) {
            controller.getPublisher().dispatch(
                    p.getPlayer().getName() + " has been killed by "
                            + this.getPlayer().getName());
            score++;
            p.moveToDestination(initialPosition);
        }
    }

    /**
     * Getter of the Score of a Player.
     *
     * @return return the number of kills made by a Player.
     *
     */
    public int getScore() {
        return score;
    }
}
