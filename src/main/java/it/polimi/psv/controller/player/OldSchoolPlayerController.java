package it.polimi.psv.controller.player;

import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.server.User;

/**
 * Player controller for Old School.
 */
public class OldSchoolPlayerController extends PlayerController {

    public OldSchoolPlayerController(Player player, GameController controller,
            User user) {
        super(player, controller, user);
    }

}
