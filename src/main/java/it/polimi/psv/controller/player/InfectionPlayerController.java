package it.polimi.psv.controller.player;

import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.server.User;

/**
 * Player Controller for Infection.
 */
public class InfectionPlayerController extends PlayerController {

    private boolean infected = false;

    public InfectionPlayerController(Player player, GameController controller,
            User user) {
        super(player, controller, user);
    }

    /**
     * Attack routine in Infection Mode. If the attacker is alien and the
     * defender is a Human without DefenseCard, the Human become an alien, and
     * restart the game from the same sector.
     *
     * @param p
     *            Player Controller of the attacker.
     */

    @Override
    protected void attackRoutine(PlayerController p) {
        Player currentPlayer = p.getPlayer();
        if (currentPlayer instanceof Human
                && currentPlayer.getObjects().contains(
                        new ObjectCard(ObjectCardType.DEFENSE))) {
            controller.getPublisher()
                    .dispatch(
                            "Player " + currentPlayer.getName()
                                    + " uses DEFENSE Card!");
            ObjectCard card = new ObjectCard(ObjectCardType.DEFENSE);
            currentPlayer.removeObject(card);
            controller.getDeckController().discardObjectCard(card);
        } else if (currentPlayer instanceof Human) {
            if (this.player instanceof Human) {
                controller.removePlayer(p);
            } else
                this.infect(p);
            controller.getPublisher().dispatch(
                    "Human " + currentPlayer.getName()
                            + " has become an alien!");
        } else {
            controller.removePlayer(p);
        }
    }

    /**
     * How the infection works.
     *
     * @param player
     *            the defender infected by an alien.
     */

    private void infect(PlayerController player) {
        player.setPlayer(new Alien(player.getPlayer().getPosition(), player
                .getPlayer().getName()));
        ((InfectionPlayerController) player).setInfected();
    }

    public void setInfected() {
        this.infected = true;
    }

    public boolean getInfected() {
        return this.infected;
    }

}
