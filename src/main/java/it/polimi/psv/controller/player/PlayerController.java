package it.polimi.psv.controller.player;

import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Logic behind the player and his possible actions.
 *
 */
public class PlayerController {

    protected Player player;
    protected final GameController controller;
    private final User user;

    /**
     * Constructor of the PlayerController.
     *
     * @param player
     *            ,controller The player associated to the Controller and the
     *            Game Controller of the current Game.
     */

    public PlayerController(Player player, GameController controller, User user) {
        this.player = player;
        this.controller = controller;
        this.player.getPosition().addPlayer(this);
        this.user = user;

    }

    /**
     * Logic for player movement. If the destination is reachable, then move the
     * player into that Sector
     * 
     * @param destination
     *            the destination of the movement.
     * @return true if player moves
     */
    public boolean movePlayer(Sector destination) {
        if (controller.checkDestination(player, destination)) {

            moveToDestination(destination);
            controller.getModel().setGameState(GameState.AFTER_MOVE);
            return true;
        }
        return false;
    }

    /**
     * the method that physically moves the player from one sector to another.
     *
     * @param destination
     *            Sector destination of the movement.
     */
    public void moveToDestination(Sector destination) {
        this.player.getPosition().removePlayer(this);
        this.player.setPosition(destination);
        this.player.getPosition().addPlayer(this);
    }

    /**
     * Gives to the caller the set of reachable Sectors.
     * 
     * @return Set<Sector> into which the player can move
     */
    public Set<Sector> getPossibleDestinations() {
        return controller.getModel().getMap()
                .getSectorsInRange(player.getPosition(), player.getSpeed());
    }

    /**
     * player attack logic
     */

    public void attack() {
        controller.getPublisher().dispatch(
                player.getName() + " attacks in this sector: "
                        + player.getPosition().toString());
        Sector sector = player.getPosition();
        if (sector.getPlayersInSector().size() == 1) {
            controller.getPublisher()
                    .dispatch("There's no one in this sector!");
        }
        Set<PlayerController> players = new HashSet<PlayerController>(
                sector.getPlayersInSector());
        players.remove(this);

        for (PlayerController p : players) {
            attackRoutine(p);
        }
        if (player instanceof Human) {
            player.removeObject(new ObjectCard(ObjectCardType.ATTACK));
        }
    }

    /**
     * How the attack works against Human and Aliens. First, checks if the
     * defender player is a Human, if so, checks if he has a Defense Card: if
     * yes, uses it and remove the card from the player's deck, else the Player
     * is eliminated. Eventually, if the attacker is an Alien he enters into
     * alien feeding mode.
     *
     * @param p
     *            Controller related to the player p.
     */

    protected void attackRoutine(PlayerController p) {
        Player currentPlayer = p.getPlayer();
        if (currentPlayer instanceof Human
                && currentPlayer.getObjects().contains(
                        new ObjectCard(ObjectCardType.DEFENSE))) {
            controller.getPublisher()
                    .dispatch(
                            "Player " + currentPlayer.getName()
                                    + " uses DEFENSE Card!");
            ObjectCard card = new ObjectCard(ObjectCardType.DEFENSE);
            currentPlayer.removeObject(card);
            controller.getDeckController().discardObjectCard(card);
        } else {
            if ((this.player instanceof Alien)
                    && (currentPlayer instanceof Human)) {
                this.alienFeeding();
            }
            controller.removePlayer(p);
        }

    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "PlayerController " + player;
    }

    public boolean hasPlayableCards() {
        if (player.getObjects().isEmpty()) {
            return false;
        }
        if (player.getObjects().size() == 1
                && player.getObjects().contains(
                        new ObjectCard(ObjectCardType.DEFENSE))) {
            return false;
        }
        return true;
    }

    public User getUser() {
        return this.user;
    }

    /**
     * if player is Alien and eats the first human, increment his speed to 3
     * 
     */
    private void alienFeeding() {
        if (this.player.getSpeed() == 2)
            this.player.incrementSpeed();
    }

    /**
     * Getter of the GameController.
     *
     * @return return the GameController
     *
     */
    public GameController getController() {
        return controller;
    }

}
