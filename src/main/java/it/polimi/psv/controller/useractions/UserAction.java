package it.polimi.psv.controller.useractions;

import java.io.Serializable;

/**
 * List of all the possible actions a player can do during his turn.
 */
public enum UserAction implements Serializable {

    MOVE, ATTACK, USE_CARD, DRAW_CARD, ENDTURN, REVEAL_IDENTITY, DO_NOT_MOVE;
}
