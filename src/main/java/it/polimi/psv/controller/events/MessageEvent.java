package it.polimi.psv.controller.events;

/**
 * Event for communicating messages from server to client. Use
 * {@code getMessage()} to get the string
 * 
 * @author alexvago
 *
 */
public class MessageEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = 1094814705483185416L;

    private final String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
