package it.polimi.psv.controller.events;

import it.polimi.psv.controller.useractions.UserAction;

import java.util.List;

public class SendPossibleActionsEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -8405262738094971765L;

    private final List<UserAction> possibleActions;

    public SendPossibleActionsEvent(List<UserAction> list) {
        this.possibleActions = list;
    }

    public List<UserAction> getPossibleActions() {
        return possibleActions;
    }

}
