package it.polimi.psv.controller.events;

import it.polimi.psv.model.cards.ObjectCardType;

public class AskForCardToUseEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -947069908055826359L;
    private ObjectCardType[] types;

    public AskForCardToUseEvent(ObjectCardType[] types) {
        this.types = types;
    }

    public ObjectCardType[] getTypes() {
        return types;
    }

}
