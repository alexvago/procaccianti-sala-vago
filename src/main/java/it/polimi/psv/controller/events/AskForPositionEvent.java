package it.polimi.psv.controller.events;

import java.util.List;

public class AskForPositionEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = 1905420521904631683L;

    private final String message;
    private final List<int[]> possibleSectors;

    public AskForPositionEvent(String message, List<int[]> possibleSectors) {
        this.message = message;
        this.possibleSectors = possibleSectors;
    }

    public String getMessage() {
        StringBuilder toReturn = new StringBuilder();
        toReturn.append(message + "\n");
        if (possibleSectors != null) {
            for (int[] coords : possibleSectors) {
                toReturn.append("[" + (char) (coords[0] + 65) + " "
                        + ((coords[0] / 2) + coords[1] + 1) + "] ");
            }
        }
        return toReturn.toString();
    }

}
