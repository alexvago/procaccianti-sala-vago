package it.polimi.psv.controller.events;

import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.model.map.MapType;

public class NotifyGameStartEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = 2764705211775222646L;
    private final MapType map;
    private final RuleType rules;

    public NotifyGameStartEvent(MapType map, RuleType rules) {
        this.map = map;
        this.rules = rules;
    }

    public MapType getMap() {
        return map;
    }

    public RuleType getRules() {
        return rules;
    }

    public String getMessage() {
        return "Game started on map " + map + " with rules " + rules;
    }

}
