package it.polimi.psv.controller.events;

public class NoiseInYourSectorEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -6984259173992632799L;
    private static final String message = "You draw a Noise In Your Sector card. Press enter to declare your position.";

    public String getMessage() {
        return message;
    }

}
