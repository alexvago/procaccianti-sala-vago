package it.polimi.psv.controller.events;

import it.polimi.psv.model.cards.ObjectCardType;

public class AskToDiscardEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -7185721103755863570L;

    private ObjectCardType[] types;

    public AskToDiscardEvent(ObjectCardType[] types) {
        this.types = types;
    }

    public ObjectCardType[] getTypes() {
        return types;
    }

}
