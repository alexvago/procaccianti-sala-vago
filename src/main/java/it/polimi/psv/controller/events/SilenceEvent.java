package it.polimi.psv.controller.events;

public class SilenceEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -2356155164248267192L;
    private static final String message = "You draw a Silence card! Press Enter to declare it";

    public String getMessage() {
        return message;
    }
}
