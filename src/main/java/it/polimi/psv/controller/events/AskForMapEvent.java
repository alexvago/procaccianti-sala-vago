package it.polimi.psv.controller.events;

import it.polimi.psv.model.map.MapType;

public class AskForMapEvent implements Event {

    /**
     * 
     */
    private static final long serialVersionUID = -8529192649324004364L;

    private MapType[] maps;

    public AskForMapEvent(MapType[] maps) {
        this.maps = maps;
    }

    public MapType[] getMaps() {
        return maps;
    }

}
