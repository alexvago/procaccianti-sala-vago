package it.polimi.psv.controller.events;

import it.polimi.psv.controller.game.RuleType;

/**
 * Event for asking the user to choose the rules for the new game.
 * 
 * @author alexvago
 *
 */
public class AskForRulesEvent implements Event {

    private static final long serialVersionUID = -377313395694856067L;

    private RuleType[] rules = RuleType.values();

    public RuleType[] getRules() {
        return rules;
    }

}
