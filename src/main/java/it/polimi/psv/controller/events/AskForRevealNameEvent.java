package it.polimi.psv.controller.events;

public class AskForRevealNameEvent implements Event {

    /**
	 * 
	 */
    private static final long serialVersionUID = -6247356989401705431L;

    private String[] names;

    public AskForRevealNameEvent(String[] names) {
        this.names = names;
    }

    public String[] getNames() {
        return names;
    }
}
