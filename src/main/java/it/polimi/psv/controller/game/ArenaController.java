package it.polimi.psv.controller.game;

import it.polimi.psv.controller.player.ArenaPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.SecureSector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.publisher.Publisher;

import java.util.HashSet;
import java.util.Set;

/**
 * Arena Controller, extension of GameController.
 */

public class ArenaController extends GameController {

    /**
     * Constructor of an Arena Controller.
     *
     * @param model
     *            The underlying model of the Game.
     */
    public ArenaController(GameModel model) {
        super(model);

    }

    public ArenaController(GameModel model, Publisher publisher,
            GameControllerThread thread) {
        super(model, publisher, thread);
    }

    /**
     * Checks if there is a player who can win the game. A player wins the game
     * if his number of kills is equal to the number of players in game. But, if
     * the 39 rounds are passed, the winner is who has reached the major number
     * of kills.
     */
    public boolean checkAliensWinningConditions() {
        for (PlayerController player : model.getPlayersInGame()) {
            if (((ArenaPlayerController) player).getScore() >= players.size()) {
                model.addWinner(player);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a destination is reachable for the player. The method
     * constructs a set of reachable sectors based on the getNeighbor method of
     * the map, but without adding Secure Sectors in it (because in this
     * scenario is forbidden to move on a Secure Sector).
     */
    @Override
    public boolean checkDestination(Player player, Sector destination) {
        Set<Sector> possibleSectors = new HashSet<>();
        for (Sector sector : model.getMap().getSectorsInRange(
                player.getPosition(), player.getSpeed())) {
            if (!(sector instanceof SecureSector)) {
                possibleSectors.add(sector);
            }
        }
        return possibleSectors.contains(destination);
    }
}
