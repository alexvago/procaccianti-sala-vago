package it.polimi.psv.controller.game;

import it.polimi.psv.controller.decks.DeckController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.publisher.Publisher;

import java.util.ArrayList;
import java.util.List;

/**
 * GameController with all the logic behind a match.
 */
public class GameController {

    protected final List<PlayerController> players; // users in game
    protected final GameModel model; // state of the game
    protected final DeckController deckController;
    protected Publisher publisher;
    protected final GameControllerThread thread;

    /**
     * Constructs a new GameController with given model and manager
     * 
     * @param model
     *            underlying GameModel
     * @param gameManager
     *            the server games manager Thread
     */
    public GameController(GameModel model) {
        this.players = new ArrayList<PlayerController>();
        this.model = model;
        this.deckController = new DeckController(model);
        this.publisher = new Publisher();
        this.thread = null;
    }

    public GameController(GameModel model, Publisher publisher,
            GameControllerThread thread) {
        this.players = new ArrayList<PlayerController>();
        this.model = model;
        this.deckController = new DeckController(model);
        this.publisher = publisher;
        this.thread = thread;
    }

    /**
     * adds a User to current Game
     * 
     * @param player
     */
    public void addPlayer(PlayerController player) {
        this.players.add(player);
        model.addPlayer(player);
    }

    /**
     * removes a player from the game
     * 
     * @param p
     *            the player died
     */

    public void removePlayer(PlayerController p) {
        this.players.remove(p);
        model.removePlayer(p);
        thread.removeUser(p.getUser(), false);
        publisher.dispatch("Player " + p.getPlayer().getName() + " is out!");
    }

    /**
     * Remove a player form the controller and the model, then publish a
     * disconnection message.
     * 
     * @param p
     */
    public void removeDisconnectedPlayer(PlayerController p) {
        this.players.remove(p);
        model.removePlayer(p);
        publisher.dispatch("Player " + p.getPlayer().getName()
                + " is disconnected!");
    }

    /**
     * Initializes the game before start.
     */
    public void initializeGame() {
        model.setGameState(GameState.BEFORE_MOVE);
    }

    /**
     * Checks if the given player can move in the given Sector destination
     * 
     * @param player
     *            Player moving
     * @param destination
     *            the Sector destination
     * @return true if destination is reachable
     */
    public boolean checkDestination(Player player, Sector destination) {
        return model.getMap()
                .getSectorsInRange(player.getPosition(), player.getSpeed())
                .contains(destination);

    }

    /**
     * Checks if a human player can escape from the the spaceship when arrives
     * on an EscapeHatchSector. If the sector's type isn't set to red, an
     * EscapeHatch card is drawn and its value is checked: if green, the player
     * wins the game, is added to winners list in the GameModel, the
     * EscapeHatchSector type is set to red; if red, the EscapeHatchSector type
     * is set to red, the card is simply discarded and the game continues
     * normally.
     * 
     * 
     * @param player
     *            the human player who's trying to escape from the spaceship
     * @return true if he did, else false
     * @throws EmptyDeckException
     */
    public boolean checkHumansWinningConditions(PlayerController player)
            throws EmptyDeckException {

        Sector position = player.getPlayer().getPosition();
        if (position instanceof EscapeHatchSector
                && !((EscapeHatchSector) position).getType().equals(
                        EscapeHatchType.RED)) {
            try {
                EscapeHatchCard card = deckController.drawEscapeHatchCard();
                if (card.type().equals(EscapeHatchType.GREEN)) {
                    publisher
                            .dispatch("Human player "
                                    + player.getPlayer().getName()
                                    + " has drawn a GREEN Escape Hatch Card! He's a winner!");
                    ((EscapeHatchSector) position).setType(EscapeHatchType.RED);
                    model.addWinner(player);
                    model.removePlayer(player);
                    this.players.remove(player);
                    deckController.discardEscapeHatchCard(card);
                    return true;
                } else {
                    publisher.dispatch("Human player "
                            + player.getPlayer().getName()
                            + " has drawn a RED Escape Hatch Card! The "
                            + position.toString()
                            + " is damaged and cannot be used to escape!");
                    ((EscapeHatchSector) position).setType(EscapeHatchType.RED);
                    deckController.discardEscapeHatchCard(card);
                    return false;
                }
            } catch (EmptyDeckException e) {
                // cannot happen
            }
        }
        return false;
    }

    /**
     * Checks if the whole alien squad can win the game. If all the 4 Escape
     * Hatches are set to red, returns true because humans cannot win. If there
     * are any humans in game, returns false.
     * 
     * @return true if the alien squad has won the game, else false
     */
    public boolean checkAliensWinningConditions() {

        List<EscapeHatchSector> redOnes = new ArrayList<EscapeHatchSector>();
        for (EscapeHatchSector sector : model.getMap().getEscapeHatces()) {
            if (sector.getType() == EscapeHatchType.RED) {
                redOnes.add(sector);
            }
        }
        if (redOnes.size() == model.getMap().getEscapeHatces().size()) {
            return true;
        }
        for (PlayerController pc : model.getPlayersInGame()) {
            if (pc.getPlayer() instanceof Human)
                return false;
        }

        return true;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    /**
     * Getter of the GameModel.
     *
     * @return return the underlying model.
     *
     */
    public GameModel getModel() {
        return model;
    }

    /**
     * Getter of the DeckController.
     *
     * @return return the DeckController of the Game.
     *
     */
    public DeckController getDeckController() {
        return deckController;
    }

}
