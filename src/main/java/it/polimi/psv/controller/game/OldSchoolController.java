package it.polimi.psv.controller.game;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.publisher.Publisher;

import java.util.ArrayList;
import java.util.List;

/**
 * Old School Controller, extension of GameController.
 */
public class OldSchoolController extends GameController {
    /**
     * Constructor of an Old School Controller.
     *
     * @param model
     *            The underlying model of the Game.
     */
    public OldSchoolController(GameModel model) {
        super(model);
    }

    public OldSchoolController(GameModel model, Publisher publisher,
            GameControllerThread thread) {
        super(model, publisher, thread);
    }

    /**
     * Initialization of a Old School Game, with Psychologist starting position
     * in Alien Sector, Soldier with the size of Object Card List set to 4 and
     * the Copilot starting with a Teleport Card.
     *
     */
    @Override
    public void initializeGame() {
        model.setGameState(GameState.BEFORE_MOVE);
        for (PlayerController pc : players) {
            if (pc.getPlayer() instanceof Human) {
                if (((Human) pc.getPlayer()).getRole()
                        .equals(HumanRole.COPILOT)) {
                    model.getObjectDeck().getGameDeck()
                            .remove(new ObjectCard(ObjectCardType.TELEPORT));
                    pc.getPlayer().addObject(
                            new ObjectCard(ObjectCardType.TELEPORT));
                }
                if (((Human) pc.getPlayer()).getRole().equals(
                        HumanRole.PSYCHOLOGIST)) {
                    pc.moveToDestination(model.getMap().getAlienSector());
                }
                if (((Human) pc.getPlayer()).getRole()
                        .equals(HumanRole.SOLDIER)) {
                    pc.getPlayer().setMaxObjects(4);

                }
            }
        }
    }

    /**
     * Checks if a human can win the game. If the Human is an Engineer, he draws
     * two EscapeHatchCards. If one of these cards is green, he wins the game,
     * else the escape hatch is closed.
     * 
     * @throws EmptyDeckException
     */
    @Override
    public boolean checkHumansWinningConditions(PlayerController player)
            throws EmptyDeckException {
        if (!((Human) player.getPlayer()).getRole().equals(HumanRole.ENGINEER)) {
            return super.checkHumansWinningConditions(player);
        } else {
            List<EscapeHatchCard> cardsDrawn = new ArrayList<EscapeHatchCard>();
            cardsDrawn.add(deckController.drawEscapeHatchCard());
            cardsDrawn.add(deckController.drawEscapeHatchCard());
            Sector position = player.getPlayer().getPosition();
            if (cardsDrawn.contains(new EscapeHatchCard(EscapeHatchType.GREEN))) {
                publisher
                        .dispatch("Human player "
                                + player.getPlayer().getName()
                                + " has drawn a GREEN Escape Hatch Card! He's a winner!");
                ((EscapeHatchSector) position).setType(EscapeHatchType.RED);
                model.addWinner(player);
                model.removePlayer(player);
                deckController.discardEscapeHatchCard(cardsDrawn.remove(1));
                deckController.discardEscapeHatchCard(cardsDrawn.remove(0));
                return true;
            } else {
                publisher.dispatch("Human player "
                        + player.getPlayer().getName()
                        + " has drawn a RED Escape Hatch Card! The "
                        + position.toString()
                        + " is damaged and cannot be used to escape!");
                ((EscapeHatchSector) position).setType(EscapeHatchType.RED);
                deckController.discardEscapeHatchCard(cardsDrawn.remove(1));
                deckController.discardEscapeHatchCard(cardsDrawn.remove(0));
                return false;
            }
        }
    }

}
