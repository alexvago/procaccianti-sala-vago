package it.polimi.psv.controller.game;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.server.GameControllerThread;
import it.polimi.psv.server.publisher.Publisher;

/**
 * Infection Controller, extension of GameController.
 */

public class InfectionController extends GameController {

    public InfectionController(GameModel model, Publisher publisher,
            GameControllerThread thread) {
        super(model, publisher, thread);
    }

    /**
     * Override of initializeGame, with all Escape Hatch Sector set to Green.
     */

    @Override
    public void initializeGame() {

        // Sets all EscapeHatchSector to GREEN
        for (Sector s : model.getMap().getMap().values()) {
            if (s instanceof EscapeHatchSector) {
                ((EscapeHatchSector) s).setType(EscapeHatchType.GREEN);
            }
        }
    }

    /**
     * Overrides the super method of the class GameController. Here, if the
     * player has reached a GREEN escape hatch sector, he becomes a winner and
     * the escape hatch type is set to RED, then returns true; false otherwise.
     */
    @Override
    public boolean checkHumansWinningConditions(PlayerController player) {

        Sector position = player.getPlayer().getPosition();
        if (position instanceof EscapeHatchSector
                && ((EscapeHatchSector) position).getType().equals(
                        EscapeHatchType.GREEN)) {
            publisher.dispatch("Human player " + player.getPlayer().getName()
                    + " has reached a GREEN Escape Hatch! He's a winner!");
            ((EscapeHatchSector) position).setType(EscapeHatchType.RED);
            model.addWinner(player);
            model.removePlayer(player);
            this.players.remove(player);
            return true;
        } else
            return false;
    }

}
