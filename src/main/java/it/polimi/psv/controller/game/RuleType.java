package it.polimi.psv.controller.game;

/**
 * Enumeration of all types of rules, including the standard game (advanced) and
 * the 3 scenarios(arena,infection and old school).
 */

public enum RuleType {

    ADVANCED, ARENA, OLDSCHOOL, INFECTION;

}
