package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.state.GameState;

import java.util.Observable;
import java.util.Observer;

/**
 * Description of how it's implemented the use of an Adrenaline Card
 *
 * @author
 *
 */
public class AdrenalineAction implements Action, Observer {

    PlayerController pc;

    /**
     * This method is called when a human player uses an Adrenaline Object Card.
     * Player's speed is incremented by one. An observer is added to the
     * ModelState. The Adrenaline card is removed from player's card list and
     * discarded.
     * 
     * @param playerController
     *            the controller associated to the player.
     */
    @Override
    public void executeAction(PlayerController playerController) {
        playerController.getPlayer().incrementSpeed();
        playerController.getController().getModel().addObserver(this);
        this.pc = playerController;
        ObjectCard cardToBeDiscard = playerController.getPlayer().removeObject(
                new ObjectCard(ObjectCardType.ADRENALINE));
        playerController.getController().getDeckController()
                .discardObjectCard(cardToBeDiscard);
    }

    /**
     * This update method is called by the notifyAll present in the GameModel.
     * When the GameState is changed into "ENDTURN", player's speed is
     * decremented by one. Then the observer is deleted.
     * 
     * @param model
     *            the observable object.
     * @param arg
     *            the argument passed to the notifyObservers method.
     */
    @Override
    public void update(Observable model, Object arg) {
        if ((GameState) arg == (GameState.ENDTURN)) {
            pc.getPlayer().decrementSpeed();
            pc.getController().getModel().deleteObserver(this);
        }

    }

}
