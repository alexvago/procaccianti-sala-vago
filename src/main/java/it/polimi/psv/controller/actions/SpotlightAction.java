package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.GameMap;
import it.polimi.psv.model.sector.Sector;

import java.util.HashSet;
import java.util.Set;

/**
 * Description of how it's implemented the use of a Spotlight Card
 *
 * @author
 *
 */
public class SpotlightAction implements Action {
    /**
     * This method is called when a human player uses a Spotlight Object Card.
     * First, ask the Player the position where he wants to put the spotlight,
     * then check if in that position and in the adjacent ones there's anyone.
     * If so, print the name of the player(s) and their relative position.
     * 
     * @param playerController
     *            the controller associated to the player.
     */

    private int x = 0, y = 0;

    @Override
    public void executeAction(PlayerController playerController) {
        Sector location = playerController.getController().getModel().getMap()
                .getSector(x, y);
        GameMap map = playerController.getController().getModel().getMap();
        Set<Sector> sectorsSet = map.getSectorsInRange(location, 1);
        sectorsSet.add(location);
        Set<PlayerController> playersSet = new HashSet<PlayerController>();
        for (Sector s : sectorsSet) {
            playersSet.addAll(s.getPlayersInSector());
        }
        for (PlayerController p : playersSet) {
            p.getController()
                    .getPublisher()
                    .dispatch(
                            p.getPlayer().getName() + " is in sector: "
                                    + p.getPlayer().getPosition().toString());
        }
        if (playersSet.size() == 0) {
            playerController.getController().getPublisher()
                    .dispatch("These sectors are empty!");
        }
        ObjectCard cardToBeDiscard = playerController.getPlayer().removeObject(
                new ObjectCard(ObjectCardType.SPOTLIGHT));
        playerController.getController().getDeckController()
                .discardObjectCard(cardToBeDiscard);

    }

    public void setCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
