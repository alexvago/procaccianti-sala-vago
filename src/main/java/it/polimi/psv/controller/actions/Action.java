package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.player.PlayerController;

/**
 * Abstract interface, implements the method executeAction.
 * 
 * @author
 */

public interface Action {

    public abstract void executeAction(PlayerController playerController);

}
