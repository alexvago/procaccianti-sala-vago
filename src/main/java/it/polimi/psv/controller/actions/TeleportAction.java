package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.player.OldSchoolPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;

/**
 * Description of how it's implemented the use of a Teleport Card
 *
 * @author
 *
 */
public class TeleportAction implements Action {

    /**
     * This method is called when a human player uses a Teleport Object Card.
     * The player is moved to the Human Sector in the game map. Then a Teleport
     * Object Card is removed from the player's objects list, and discarded in
     * the ObjectDeck graveyard.
     * 
     * @param playerController
     *            the controller associated to the player.
     */
    @Override
    public void executeAction(PlayerController playerController) {
        playerController.moveToDestination(playerController.getController()
                .getModel().getMap().getHumanSector());
        if (playerController instanceof OldSchoolPlayerController
                && ((Human) playerController.getPlayer()).getRole().equals(
                        HumanRole.PILOT)) {
            playerController.getPlayer().incrementPilotsNumberOfTeleports();
            if (playerController.getPlayer().getPilotsNumberOfTeleports() == 3) {
                discardTeleportCard(playerController);
                playerController.getPlayer().resetPilotsNumberOfTeleports();
            }
        } else {
            discardTeleportCard(playerController);
        }
    }

    /**
     * Remove a Teleport Card from the Player's Object Card List and put it in
     * the graveyard.
     * 
     * @param playerController
     */
    private void discardTeleportCard(PlayerController playerController) {
        ObjectCard cardToBeDiscard = playerController.getPlayer().removeObject(
                new ObjectCard(ObjectCardType.TELEPORT));
        playerController.getController().getDeckController()
                .discardObjectCard(cardToBeDiscard);
    }

}
