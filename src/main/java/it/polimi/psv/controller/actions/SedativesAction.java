package it.polimi.psv.controller.actions;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.state.GameState;

import java.util.Observable;
import java.util.Observer;

/**
 * Description of how it's implemented the use of an Sedatives Card
 *
 * @author
 *
 */
public class SedativesAction implements Action, Observer {

    private PlayerController pc;

    /**
     * This method is called when a human player uses a Sedatives Object Card.
     * Player's isUnderSedatives attribute is set at true. An observer is added
     * to the ModelState. The Sedatives card is removed from player's card list
     * and discarded.
     * 
     * @param playerController
     *            the controller associated to the player.
     */
    @Override
    public void executeAction(PlayerController playerController) {
        playerController.getPlayer().setUnderSedatives(true);
        playerController.getController().getModel().addObserver(this);
        this.pc = playerController;
        ObjectCard cardToBeDiscard = playerController.getPlayer().removeObject(
                new ObjectCard(ObjectCardType.SEDATIVES));
        playerController.getController().getDeckController()
                .discardObjectCard(cardToBeDiscard);
    }

    /**
     * This update method is called by the notifyAll present in the GameModel.
     * When the GameState is changed into "ENDTURN", the sedatives effect runs
     * off. Then the observer is deleted.
     * 
     * @param model
     *            the observable object.
     * @param arg
     *            the argument passed to the notifyObservers method.
     */
    @Override
    public void update(Observable model, Object arg) {
        if ((GameState) arg == (GameState.ENDTURN)) {
            pc.getPlayer().setUnderSedatives(false);
            pc.getController().getModel().deleteObserver(this);
        }

    }

}
