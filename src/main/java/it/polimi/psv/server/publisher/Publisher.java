package it.polimi.psv.server.publisher;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Publisher of Publisher-Subscriber architecture. Registers all subscribers in
 * a List and dispatches messagges to all of them
 * 
 * @author alexvago
 *
 */
public class Publisher {

    private List<Socket> subscribers;

    public Publisher() {
        this.subscribers = new ArrayList<Socket>();
    }

    /**
     * add a subscriber to the List
     * 
     * @param subscriber
     */
    public void addSubscriber(Socket subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * Remove a subscriber from the list
     * 
     * @param subscriber
     */
    public void removeSubscriber(Socket subscriber) {
        subscribers.remove(subscriber);
    }

    /**
     * Method used to send a message to all the subscriber of the publisher.
     * 
     * @param string
     *            The string sent to the subscribers.
     */
    public void dispatch(String string) {
        for (Socket s : subscribers) {
            try {
                PrintWriter out = new PrintWriter(s.getOutputStream());
                out.println(string);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
