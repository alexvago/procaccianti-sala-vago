package it.polimi.psv.server;

import it.polimi.psv.controller.events.AskForPositionEvent;
import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.player.ArenaPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.Sector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Asks from what Escape Hatch Sector the Aliens want to start.
 */
public class ArenaAskForInitialPositionThread implements
        Callable<PlayerController> {

    User user;
    Player player;
    GameController controller;

    public ArenaAskForInitialPositionThread(Player player, User user,
            GameController controller) {
        this.user = user;
        this.player = player;
        this.controller = controller;
    }

    /**
     * First, prints to the current user the possible options, then checks if
     * the coordinates inserted by the user are correct. If correct, set the
     * user's initial position to that sector.
     */
    @Override
    public PlayerController call() throws Exception {

        Sector initSector = null;
        List<EscapeHatchSector> escapeHatches = controller.getModel().getMap()
                .getEscapeHatces();
        List<int[]> possibleCoordinates = new ArrayList<>();
        for (int i = 0; i < escapeHatches.size(); i++) {
            Sector s = escapeHatches.get(i);
            int[] array = new int[2];
            array[0] = s.getX();
            array[1] = s.getY();
            possibleCoordinates.add(array);
        }
        do {
            user.writeObject(new AskForPositionEvent(
                    "Insert coordinates of starting Sector",
                    (ArrayList<int[]>) possibleCoordinates));
            int[] coords = ((int[]) user.readObject());
            initSector = controller.getModel().getMap()
                    .getSector(coords[0], coords[1]);
        } while (!(initSector instanceof EscapeHatchSector));
        user.writeObject(new MessageEvent("You'll start from " + initSector
                + ". Waiting for other players..."));
        PlayerController playerController = new ArenaPlayerController(player,
                controller, user, initSector);
        user.setPlayer(playerController);
        return playerController;
    }
}
