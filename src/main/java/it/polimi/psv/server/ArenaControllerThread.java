package it.polimi.psv.server;

import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.game.ArenaController;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.player.ArenaPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.SecureSector;
import it.polimi.psv.model.state.GameState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ArenaControllerThread extends GameControllerThread {

    public ArenaControllerThread(MapType mapType) {
        super(mapType);
        controller = new ArenaController(model, publisher, this);
    }

    /**
     * Method to assign the initial positions of the Players in an Arena Match.
     * The Players(Aliens) decide from which Escape Hatch Sector they will
     * start.
     */
    @Override
    protected void assignCharacters() {

        ExecutorService executor = Executors.newCachedThreadPool();
        List<Future<PlayerController>> futures;
        List<Callable<PlayerController>> callables = new ArrayList<Callable<PlayerController>>();

        for (int i = 0; i < users.size(); i++) {

            Player player = new Alien(model.getMap().getAlienSector(), users
                    .get(i).getName());
            callables.add(new ArenaAskForInitialPositionThread(player, users
                    .get(i), controller));
        }

        try {
            futures = executor.invokeAll(callables);
            for (Future<PlayerController> future : futures) {
                try {
                    controller.addPlayer(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    LOGGER.print("Cannot get playerController from Future");
                    LOGGER.print(e);
                }
            }
        } catch (InterruptedException e1) {
            LOGGER.print("Cannot execute all callables, InterruptException");
            LOGGER.print(e1);
        }
    }

    /**
     * Remove the secure sectors from the possible destinations, see Arena
     * Rules.
     */
    @Override
    protected List<Sector> getPossibleSectors(User currentUser) {
        List<Sector> possibleSectors = new ArrayList<Sector>(currentUser
                .getPlayerController().getPossibleDestinations());
        List<Sector> secureSectors = new ArrayList<Sector>();
        for (Sector s : possibleSectors) {
            if (s instanceof SecureSector) {
                secureSectors.add(s);
            }
        }
        possibleSectors.removeAll(secureSectors);

        return possibleSectors;
    }

    /**
     * Print the number of kills of every player at the start of every round.
     */
    @Override
    protected void roundStartMessage(User currentUser) throws IOException {
        currentUser.writeObject(new MessageEvent(currentUser
                + " "
                + currentUser.getPlayerController().getPlayer().getPosition()
                + "\nKills: "
                + ((ArenaPlayerController) currentUser.getPlayerController())
                        .getScore()));
        if (model.getGameState().equals(GameState.BEFORE_MOVE)) {
            StringBuilder message = new StringBuilder();
            message.append("Scores\n");
            for (User user : users) {
                message.append(user.getName() + ":\t");
                message.append(((ArenaPlayerController) user
                        .getPlayerController()).getScore());
                message.append("\n");
            }
            currentUser.writeObject(new MessageEvent(message.toString()));
        }

    }

    @Override
    protected void notifyGameStartToUsers() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\nUsers in game:\n");
        for (User u : users) {
            u.writeObject(new NotifyGameStartEvent(map, RuleType.ARENA));
            builder.append(u.getName() + "\n");
        }
        publisher.dispatch(builder.toString());

    }

    @Override
    protected void attackLogic(User currentUser) {
        currentUser.getPlayerController().attack();
        if (controller.checkAliensWinningConditions()) {
            publisher.dispatch(currentUser.getName() + " has won the game");
            removeUser(currentUser, true);

            for (PlayerController player : model.getPlayersInGame()) {
                if (player.getPlayer() instanceof Alien
                        && !(player.getUser().equals(currentUser))) {
                    removeUser(player.getUser(), false);
                }

            }
            model.setGameState(GameState.GAME_END);
        } else
            model.setGameState(GameState.AFTER_ATTACK);
    }
}
