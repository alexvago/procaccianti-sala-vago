package it.polimi.psv.server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class UserOutHandler implements Runnable {

    private final ObjectOutputStream output;

    public UserOutHandler(Socket socket) throws IOException {
        this.output = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {

    }

    /**
     * Writes Object arg on the stream
     * 
     * @param arg
     * @throws IOException
     */
    public void writeObject(Object arg) throws IOException {
        output.writeObject(arg);
        output.flush();
    }

}
