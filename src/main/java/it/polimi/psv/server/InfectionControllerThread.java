package it.polimi.psv.server;

import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.game.InfectionController;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.player.InfectionPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * GameControllerThread for Infection game
 * 
 * @author alexvago
 *
 */
public class InfectionControllerThread extends GameControllerThread {

    public InfectionControllerThread(MapType mapType) {
        super(mapType);
        controller = new InfectionController(model, publisher, this);

    }

    /**
     * initialize the player controller with an infection player controller.
     */
    @Override
    protected void assignCharacters() {
        Collections.shuffle(users);
        List<HumanRole> roles = new ArrayList<HumanRole>(
                Arrays.asList(HumanRole.values()));
        Collections.shuffle(roles);
        for (int i = 0; i < users.size(); i++) {
            if (i % 2 == 0) {
                Player player = new Alien(model.getMap().getAlienSector(),
                        users.get(i).getName());
                PlayerController playerController = new InfectionPlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            } else {
                Player player = new Human(model.getMap().getHumanSector(),
                        users.get(i).getName(), roles.remove(0));
                PlayerController playerController = new InfectionPlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            }
        }
        Collections.shuffle(users);
    }

    /**
     * When an infected Human wins the game, print that is a loser. For other
     * explanations, see the overridden method.
     */
    @Override
    public void removeUser(User user, boolean isWinner) {
        try {
            if (isWinner) {
                if (((InfectionPlayerController) user.getPlayerController())
                        .getInfected()) {
                    user.writeObject(new MessageEvent(
                            "You are an infected human, unfortunately you lost the game! D:"));
                } else {
                    user.writeObject(new MessageEvent(
                            "You win!!! Congratulations! :D"));
                }
            } else
                user.writeObject(new MessageEvent("Game over!"));
        } catch (IOException e) {
            log("Cannot write messages to user!");
            log(e);
        }
        publisher.removeSubscriber(user.getSubscriberSocket());
        synchronized (users) {
            log(users.remove(user));
            turns.remove(user);
            index--;

        }
    }

    protected void notifyGameStartToUsers() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\nUsers in game:\n");
        for (User u : users) {
            u.writeObject(new NotifyGameStartEvent(map, RuleType.INFECTION));
            builder.append(u.getName() + "\n");
        }
        publisher.dispatch(builder.toString());

    }

}
