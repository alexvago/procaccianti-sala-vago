package it.polimi.psv.server;

import it.polimi.psv.controller.actions.Action;
import it.polimi.psv.controller.actions.AdrenalineAction;
import it.polimi.psv.controller.actions.SedativesAction;
import it.polimi.psv.controller.actions.SpotlightAction;
import it.polimi.psv.controller.actions.TeleportAction;
import it.polimi.psv.controller.events.AskForCardToUseEvent;
import it.polimi.psv.controller.events.AskForPositionEvent;
import it.polimi.psv.controller.events.AskForRevealNameEvent;
import it.polimi.psv.controller.events.AskToDiscardEvent;
import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.events.SendPossibleActionsEvent;
import it.polimi.psv.controller.game.OldSchoolController;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.player.OldSchoolPlayerController;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.controller.useractions.UserAction;
import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.cards.SilenceCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.DangerousSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.VoidSector;
import it.polimi.psv.model.state.GameState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * GameControllerThread of Old School game
 * 
 * @author alexvago
 *
 */
public class OldSchoolControllerThread extends GameControllerThread {

    public OldSchoolControllerThread(MapType mapType) {
        super(mapType);
        controller = new OldSchoolController(model, publisher, this);
    }

    /**
     * Print "user uses card blablabla" only is the card used is Defense or
     * Teleport.
     */
    @Override
    protected void useCardLogic(User currentUser) {
        List<ObjectCard> objects = currentUser.getPlayerController()
                .getPlayer().getObjects();
        List<ObjectCardType> types = new ArrayList<ObjectCardType>();
        for (int i = 0; i < objects.size(); i++) {
            if (!(objects.get(i).getType() == ObjectCardType.DEFENSE))
                types.add(objects.get(i).getType());
        }
        ObjectCardType[] arrayTypes = types.toArray(new ObjectCardType[types
                .size()]);
        log(currentUser + " can use: " + Arrays.toString(arrayTypes));

        try {
            currentUser.writeObject(new AskForCardToUseEvent(types
                    .toArray(new ObjectCardType[types.size()])));
            ObjectCardType cardChoosen = (ObjectCardType) currentUser
                    .readObject();
            Action action;
            switch (cardChoosen) {
            case ADRENALINE:
                action = new AdrenalineAction();
                action.executeAction(currentUser.getPlayerController());
                break;
            case SEDATIVES:
                action = new SedativesAction();
                action.executeAction(currentUser.getPlayerController());
                break;
            case TELEPORT:
                action = new TeleportAction();
                publisher.dispatch("Human " + currentUser.getName() + " uses "
                        + cardChoosen + " card!");
                action.executeAction(currentUser.getPlayerController());
                break;
            case SPOTLIGHT:
                action = new SpotlightAction();
                Sector position = null;
                int[] coordinates = null;
                do {
                    currentUser.writeObject(new AskForPositionEvent(
                            "Insert valid coordinates for Spotlight (A 1):",
                            null));
                    coordinates = (int[]) currentUser.readObject();
                    position = model.getMap().getSector(coordinates[0],
                            coordinates[1]);
                } while (position == null || (position instanceof VoidSector));
                ((SpotlightAction) action).setCoordinates(coordinates[0],
                        coordinates[1]);
                action.executeAction(currentUser.getPlayerController());
                break;
            case ATTACK:
                currentUser.getPlayerController().attack();
                break;
            default:
                break;
            }

        } catch (IOException | ClassNotFoundException e) {
            log("Cannot write or read object to/from user");
            log(e);
        }
    }

    /**
     * Initialize Player Controller as Old School Player Controller.
     */
    @Override
    protected void assignCharacters() {
        Collections.shuffle(users);
        List<HumanRole> roles = new ArrayList<HumanRole>(
                Arrays.asList(HumanRole.values()));
        Collections.shuffle(roles);
        for (int i = 0; i < users.size(); i++) {
            if (i % 2 == 0) {
                Player player = new Alien(model.getMap().getAlienSector(),
                        users.get(i).getName());
                PlayerController playerController = new OldSchoolPlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            } else {
                Player player = new Human(model.getMap().getHumanSector(),
                        users.get(i).getName(), roles.remove(0));
                PlayerController playerController = new OldSchoolPlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            }
        }
        Collections.shuffle(users);
    }

    /**
     * Override the super method with the modification of the message at the
     * beginning of every turn. A Medic can now use "Reveal Identity" and the
     * Executive Officer "Do Not Move".
     */
    @Override
    public void startGame() {
        GameManager.notifyGameStart(this);

        try {
            notifyGameStartToUsers();
        } catch (IOException e1) {
            LOGGER.print("Cannot notify users of game start");
            LOGGER.print(e1);
        }

        assignCharacters();
        turns.addAll(users);
        controller.initializeGame();

        while (!(model.getGameState() == GameState.GAME_END)) {

            User indexUser = users.get(index);
            User currentUser = turns.peek();
            model.setCurrentPlayer(currentUser.getPlayerController()
                    .getPlayer());
            model.setGameState(GameState.BEFORE_MOVE);
            publisher.dispatch("Round " + model.getRoundNumber() + ", it's "
                    + currentUser.getName() + " turn.");
            try {
                currentUser.activate(); // activates the user interaction
                while (model.getGameState() != GameState.ENDTURN
                        && model.getGameState() != GameState.GAME_END) {

                    currentUser
                            .writeObject(new MessageEvent(
                                    ""
                                            + (currentUser
                                                    .getPlayerController()
                                                    .getPlayer() instanceof Human ? ((Human) currentUser
                                                    .getPlayerController()
                                                    .getPlayer()).getRole()
                                                    + " " : "")
                                            + currentUser
                                            + " "
                                            +

                                            currentUser.getPlayerController()
                                                    .getPlayer().getPosition()));

                    List<UserAction> possibleActions = new ArrayList<>();

                    switch (model.getGameState()) {

                    case BEFORE_MOVE:
                        possibleActions.add(UserAction.MOVE);
                        addUseCard(currentUser, possibleActions);
                        addDoNotMove(currentUser, possibleActions);
                        addRevealIdentity(currentUser, possibleActions);
                        break;

                    case BEFORE_DRAW:
                        possibleActions.add(UserAction.DRAW_CARD);
                        addAttack(currentUser, possibleActions);
                        addRevealIdentity(currentUser, possibleActions);
                        break;

                    case AFTER_MOVE:
                        possibleActions.add(UserAction.ENDTURN);
                        addUseCard(currentUser, possibleActions);
                        addAttack(currentUser, possibleActions);
                        addRevealIdentity(currentUser, possibleActions);
                        break;

                    case AFTER_ATTACK:
                        possibleActions.add(UserAction.ENDTURN);
                        addUseCard(currentUser, possibleActions);
                        addRevealIdentity(currentUser, possibleActions);
                        break;

                    default:
                        break;
                    }
                    log("Sending possible actions to user " + currentUser);
                    currentUser.writeObject(new SendPossibleActionsEvent(
                            possibleActions));
                    UserAction actionChoosen = (UserAction) currentUser
                            .readObject();
                    log("user wants to " + actionChoosen.name());
                    executeAction(actionChoosen, currentUser);

                }
            } catch (IOException e) {
                log("Cannot communicate with " + currentUser);
                log(e);
            } catch (ClassNotFoundException e) {
                log("Cannot read obhject from: " + currentUser);
                log(e);
            }
            if(users.size()>0){
            	turns.offer(turns.poll());
            }
            index = users.indexOf(indexUser);
            index = (index >= users.size() - 1) ? 0 : index + 1;
            if (index == 0)
                model.incrementRoundNumber();
            if (model.getRoundNumber() == 40) {
                publisher.dispatch("Time's up, Aliens win!");
                for (PlayerController player : model.getPlayersInGame()) {
                    if (player.getPlayer() instanceof Alien) {
                        removeUser(player.getUser(), true);
                    } else
                        removeUser(player.getUser(), false);
                }

            }

        }
    }

    /**
     * Switch case that, for each action chosen, call the logic of that action.
     */
    @Override
    protected void executeAction(UserAction actionChoosen, User currentUser) {

        switch (actionChoosen) {

        case MOVE:
            moveLogic(currentUser);
            break;

        case DRAW_CARD:
            drawCardLogic(currentUser);
            break;

        case ATTACK:
            attackLogic(currentUser);
            break;

        case ENDTURN:
            endTurnLogic(currentUser);
            break;

        case USE_CARD:
            useCardLogic(currentUser);
            break;

        case DO_NOT_MOVE:
            doNotMoveLogic(currentUser);
            break;

        case REVEAL_IDENTITY:
            revealIdentityLogic(currentUser);
            break;

        default:
            break;
        }

    }

    /**
     * Add do not move to the possible action list if the human role is
     * Executive Officer.
     * 
     * @param currentUser
     * @param possibleActions
     */
    private void addDoNotMove(User currentUser, List<UserAction> possibleActions) {
        Player player = currentUser.getPlayerController().getPlayer();
        if (player instanceof Human) {
            if (((Human) player).getRole().equals(HumanRole.EXECUTIVEOFFICER)
                    && !(player.isNotMoved())) {
                possibleActions.add(UserAction.DO_NOT_MOVE);
            }
        }
    }

    /**
     * Add reveal identity to the possible action list if the human role is
     * Medic.
     * 
     * @param currentUser
     * @param possibleActions
     */
    private void addRevealIdentity(User currentUser,
            List<UserAction> possibleActions) {
        if (currentUser.getPlayerController().getPlayer() instanceof Human
                && !(currentUser.getPlayerController().getPlayer()
                        .isIdentityRevealed())) {
            if (((Human) currentUser.getPlayerController().getPlayer())
                    .getRole().equals(HumanRole.MEDIC)) {
                possibleActions.add(UserAction.REVEAL_IDENTITY);
            }
        }

    }

    /**
     * Print the list of all the players in the current game, then the current
     * player chooses the player he wants to reveal the identity. The identity
     * is printed in the Publisher. IdentityRevealed is set to true.
     * 
     * @param currentUser
     */
    private void revealIdentityLogic(User currentUser) {
        List<String> playersName = new ArrayList<String>();
        for (User user : users) {
            if (user != currentUser)
                playersName.add(user.getName());
        }
        try {
            currentUser.writeObject(new AskForRevealNameEvent(playersName
                    .toArray(new String[playersName.size()])));
            String nameChosen = (String) currentUser.readObject();
            for (User user : users) {
                if (user.getName().equals(nameChosen)) {
                    publisher
                            .dispatch("The doctor wants to know someone's identity... "
                                    + nameChosen
                                    + " is "
                                    + (user.getPlayerController().getPlayer() instanceof Human ? "a Human"
                                            : "an Alien"));

                }
            }
            currentUser.getPlayerController().getPlayer()
                    .setIdentityRevealed(true);
        } catch (IOException | ClassNotFoundException e) {
            log("Cannot write or read object to/from user");
            log(e);
        }

    }

    /**
     * If the Player is an Executive Officer, when he uses Do Not Move the
     * GameState is set to After Move, preventing him from moving.NotMoved is
     * set to true.
     * 
     * @param currentUser
     */
    private void doNotMoveLogic(User currentUser) {
        try {
            currentUser
                    .writeObject(new MessageEvent(
                            "You have used your ability! Your position has not changed."));
            currentUser.getPlayerController().getPlayer().setNotMoved(true);
            model.setGameState(GameState.AFTER_MOVE);
        } catch (IOException e) {
            log("Cannot write message event to user");
            log(e);
        }
    }

    /**
     * A Player draw an object card only if he draws a silence card.
     */
    @Override
    protected void drawObjectCardLogic(User currentUser,
            DangerousSectorCard cardDrawn) {
        if (cardDrawn instanceof SilenceCard) {
            try {
                ObjectCard objectCard = controller.getDeckController()
                        .drawObjectCard();
                Player player = currentUser.getPlayerController().getPlayer();
                player.addObject(objectCard);
                currentUser.writeObject(new MessageEvent(
                        "You draw an object card: " + objectCard));
                if (player.getObjects().size() > player.getMaxObjects()) {
                    List<ObjectCard> objects = currentUser
                            .getPlayerController().getPlayer().getObjects();
                    ObjectCardType[] types = new ObjectCardType[objects.size()];
                    for (int i = 0; i < objects.size(); i++) {
                        types[i] = objects.get(i).getType();
                    }
                    currentUser.writeObject(new AskToDiscardEvent(types));
                    ObjectCardType cardToDiscard = (ObjectCardType) currentUser
                            .readObject();
                    player.removeObject(new ObjectCard(cardToDiscard));
                    controller.getDeckController().discardObjectCard(
                            new ObjectCard(cardToDiscard));
                }
            } catch (EmptyDeckException e) {
                // non accadrà mai
            } catch (IOException e) {
                log("cannot write MessageEvent!");
                log(e);
            } catch (ClassNotFoundException e) {
                log("cannot write MessageEvent!");
                log(e);
            }
        }
    }

    /**
     * Check if a Player is Human with role set as Captain, then checks if this
     * is the first time that he moves on a dangerous sector, in that case he
     * doesn't draw a card. Finally, sets CaptainPassedOnFirstDangerous at value
     * true.
     */
    @Override
    protected boolean movedInDangerousSector(User currentUser) {
        Player player = currentUser.getPlayerController().getPlayer();
        if (player instanceof Human) {
            if (((Human) player).getRole().equals(HumanRole.CAPTAIN)
                    && !(player.isCaptainPassedOnFirstDangerous())) {
                player.setCaptainPassedOnFirstDangerous(true);
                return false;
            }
        }
        if (!(player.isUnderSedatives())
                && player.getPosition() instanceof DangerousSector) {
            return true;
        }
        return false;
    }

    @Override
    protected void notifyGameStartToUsers() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\nUsers in game:\n");
        for (User u : users) {
            u.writeObject(new NotifyGameStartEvent(map, RuleType.OLDSCHOOL));
            builder.append(u.getName() + "\n");
        }
        publisher.dispatch(builder.toString());

    }

}
