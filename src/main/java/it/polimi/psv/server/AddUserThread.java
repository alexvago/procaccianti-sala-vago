package it.polimi.psv.server;

import it.polimi.psv.controller.events.AskForMapEvent;
import it.polimi.psv.controller.events.AskForRulesEvent;
import it.polimi.psv.controller.events.Event;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.view.logger.CLILogger;
import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * When a user joins a game, this thread adds him to the game.
 */
public class AddUserThread implements Runnable {

    private static final Logger LOGGER = new CLILogger();
    private Socket clientSocket;
    private Socket subscriberSocket;
    private ExecutorService executor;
    private static Object lock1 = new Object();

    public AddUserThread(Socket clientSocket, Socket subscriberSocket,
            ExecutorService executor) {
        this.clientSocket = clientSocket;
        this.subscriberSocket = subscriberSocket;
        this.executor = executor;
    }

    /**
     * Check if there's a game in the waiting list; if the list is empty asks
     * the user what kind of rules and map he wants to use, then add the game in
     * the waiting game list. Else, add the user to the game waiting to start.
     */
    @Override
    public void run() {
        /**
         * get user name
         */

        User user = null;
        try {
            user = new User(clientSocket, subscriberSocket, executor);
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.print("Cannot create user");
            LOGGER.print(e);
        }
        LOGGER.print(user + "user created.");

        synchronized (lock1) {
            if (GameManager.getWaitingGames().isEmpty()) {

                RuleType rules = askForRules(user);
                MapType map = askForMap(user, rules);

                GameControllerThread game;
                switch (rules) {
                case ARENA:
                    game = new ArenaControllerThread(map);
                    break;

                case INFECTION:
                    game = new InfectionControllerThread(map);
                    break;
                case OLDSCHOOL:
                    game = new OldSchoolControllerThread(map);
                    break;
                default:
                    game = new GameControllerThread(map);
                    break;
                }
                executor.submit(game);

                LOGGER.print("Created new game with rules " + rules
                        + " and map " + map);

                GameManager.addGame(game);
                game.addUser(user);

            } else {
                GameManager.addUserToGame(user);
            }
        }

    }

    /**
     * Ask the user what kind of rules he wants to use.
     * 
     * @param user
     *            The user who decide the rules.
     * @return RuleType The chosen rule set.
     */
    private RuleType askForRules(User user) {
        Event event = new AskForRulesEvent();
        RuleType rules;
        try {
            user.writeObject(event);
            rules = RuleType.values()[(int) user.readObject()];

        } catch (IOException e) {
            LOGGER.print("Cannot get rules from user, choosing default value: ADVANCED");
            LOGGER.print(e);
            rules = RuleType.ADVANCED;
        } catch (ClassNotFoundException e) {
            LOGGER.print("Cannot get rules from user, choosing default value: ADVANCED");
            LOGGER.print(e);
            rules = RuleType.ADVANCED;
        }
        return rules;
    }

    /**
     * Ask the user what kind of map he wants to use.
     * 
     * @param user
     *            The user who decide the map.
     * @return MapType The chosen map type.
     */
    private MapType askForMap(User user, RuleType rules) {

        List<MapType> mapsList = new ArrayList<MapType>(Arrays.asList(MapType
                .values()));
        MapType[] maps = MapType.values();
        if (rules == RuleType.ARENA) {
            mapsList.remove(MapType.FERMI);
            maps = mapsList.toArray(new MapType[mapsList.size()]);

        }
        Event event = new AskForMapEvent(maps);
        MapType map;
        try {
            user.writeObject(event);
            map = maps[(int) user.readObject()];

        } catch (IOException e) {
            LOGGER.print("Cannot get map from user, choosing default value: GALILEI");
            LOGGER.print(e);
            map = MapType.GALILEI;
        } catch (ClassNotFoundException e) {
            LOGGER.print("Cannot get map from user, choosing default value: GALILEI");
            LOGGER.print(e);
            map = MapType.GALILEI;
        }
        return map;

    }

}
