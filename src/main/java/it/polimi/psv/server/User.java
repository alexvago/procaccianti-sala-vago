package it.polimi.psv.server;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.view.logger.CLILogger;
import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

/**
 * Handles communications with each client
 * 
 * @author alexvago
 *
 */
public class User {

    private static final Logger LOGGER = new CLILogger();
    private String name;
    private PlayerController player;
    private UserInHandler input;
    private UserOutHandler output;
    private Socket subscriberSocket;

    public User(Socket socket, Socket subscriberSocket, ExecutorService executor)
            throws IOException, ClassNotFoundException {

        this.subscriberSocket = subscriberSocket;
        this.output = new UserOutHandler(socket);
        executor.submit(this.output);
        this.input = new UserInHandler(socket);
        executor.submit(this.input);

        this.name = (String) input.readObject();
    }

    public User() {
        this.name = "Demo";
    }

    public void activate() throws IOException {
        LOGGER.print("Activate player " + name);

    }

    public void setPlayer(PlayerController playerController) {
        this.player = playerController;
    }

    public PlayerController getPlayerController() {
        return this.player;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "User " + name;
    }

    public void writeObject(Object arg) throws IOException {
        if (output != null)
            output.writeObject(arg);

    }

    public Object readObject() throws ClassNotFoundException, IOException {
        return input.readObject();
    }

    public Socket getSubscriberSocket() {
        return subscriberSocket;
    }

}
