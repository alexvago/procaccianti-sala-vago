package it.polimi.psv.server;

import it.polimi.psv.controller.actions.Action;
import it.polimi.psv.controller.actions.AdrenalineAction;
import it.polimi.psv.controller.actions.SedativesAction;
import it.polimi.psv.controller.actions.SpotlightAction;
import it.polimi.psv.controller.actions.TeleportAction;
import it.polimi.psv.controller.events.AskForCardToUseEvent;
import it.polimi.psv.controller.events.AskForPositionEvent;
import it.polimi.psv.controller.events.AskToDiscardEvent;
import it.polimi.psv.controller.events.EndGameEvent;
import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NoiseInYourSectorEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.events.SendPossibleActionsEvent;
import it.polimi.psv.controller.events.SilenceEvent;
import it.polimi.psv.controller.game.GameController;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.controller.useractions.UserAction;
import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.NoiseInYourSector;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.cards.SilenceCard;
import it.polimi.psv.model.decks.EmptyDeckException;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Alien;
import it.polimi.psv.model.player.Human;
import it.polimi.psv.model.player.HumanRole;
import it.polimi.psv.model.player.Player;
import it.polimi.psv.model.sector.AlienSector;
import it.polimi.psv.model.sector.DangerousSector;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.EscapeHatchType;
import it.polimi.psv.model.sector.HumanSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.VoidSector;
import it.polimi.psv.model.state.GameModel;
import it.polimi.psv.model.state.GameState;
import it.polimi.psv.server.publisher.Publisher;
import it.polimi.psv.view.logger.CLILogger;
import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GameControllerThread extends Observable implements Runnable {

    private static int COUNTER = 0;
    private final int currentGame;

    protected MapType map;
    protected static final Logger LOGGER = new CLILogger();
    protected List<User> users;
    protected Queue<User> turns;

    protected GameController controller;
    protected GameModel model;
    protected Publisher publisher;

    protected int index = 0;
    protected Timer timer = new Timer();

    public GameControllerThread(MapType mapType) {
        this.map = mapType;
        currentGame = COUNTER;
        users = new ArrayList<User>();
        turns = new ConcurrentLinkedQueue<User>();
        model = new GameModel(mapType);
        this.publisher = new Publisher();
        controller = new GameController(model, publisher, this);
        COUNTER++;

    }

    @Override
    public void run() {

    }

    /**
     * Describe the initialization of a game, all the phases of a turn and what
     * a player does during every phase.
     */
    public void startGame() {
        GameManager.notifyGameStart(this);

        try {
            notifyGameStartToUsers();
        } catch (IOException e1) {
            LOGGER.print("Cannot notify users of game start");
            LOGGER.print(e1);
        }

        assignCharacters();
        turns.addAll(users);
        controller.initializeGame();

        while (!(model.getGameState() == GameState.GAME_END)) {

            if (users.size() == 0) {
                log("Game end, no players");
                break;
            } else if (users.size() == 1) {
                User user = users.get(0);
                removeUser(user, true);
                try {
                    user.writeObject(new EndGameEvent());
                } catch (IOException e) {
                    log("Cannot send GameEndEvent to " + user.toString());
                    log(e);
                    removeDisconnectedUser(user);
                }
                break;
            }
            User indexUser = users.get(index);
            User currentUser = turns.peek();
            model.setCurrentPlayer(currentUser.getPlayerController()
                    .getPlayer());
            model.setGameState(GameState.BEFORE_MOVE);
            publisher.dispatch("Round " + model.getRoundNumber() + ", it's "
                    + currentUser.getName() + " turn.");
            try {
                currentUser.activate(); // activates the user interaction
                while (model.getGameState() != GameState.ENDTURN
                        && model.getGameState() != GameState.GAME_END) {

                    roundStartMessage(currentUser);
                    List<UserAction> possibleActions = new ArrayList<>();

                    switch (model.getGameState()) {

                    case BEFORE_MOVE:
                        possibleActions.add(UserAction.MOVE);
                        addUseCard(currentUser, possibleActions);
                        break;

                    case BEFORE_DRAW:
                        possibleActions.add(UserAction.DRAW_CARD);
                        addAttack(currentUser, possibleActions);
                        break;

                    case AFTER_MOVE:
                        possibleActions.add(UserAction.ENDTURN);
                        addUseCard(currentUser, possibleActions);
                        addAttack(currentUser, possibleActions);
                        break;

                    case AFTER_ATTACK:
                        possibleActions.add(UserAction.ENDTURN);
                        addUseCard(currentUser, possibleActions);
                        break;

                    default:
                        break;
                    }
                    log("Sending possible actions to user " + currentUser);
                    log(model.getGameState() + " " + possibleActions);
                    currentUser.writeObject(new SendPossibleActionsEvent(
                            possibleActions));
                    UserAction actionChoosen = (UserAction) currentUser
                            .readObject();
                    log("user wants to " + actionChoosen.name());
                    executeAction(actionChoosen, currentUser);

                }
            } catch (IOException e) {
                log("Cannot communicate with " + currentUser);
                log(e);
                removeDisconnectedUser(currentUser);
            } catch (ClassNotFoundException e) {
                log("Cannot read obhject from: " + currentUser);
                log(e);
                removeDisconnectedUser(currentUser);
            }
            if(users.size()>0){
            	turns.offer(turns.poll());
            }
            index = users.indexOf(indexUser);
            index = (index >= users.size() - 1) ? 0 : index + 1;
            if (index == 0)
                model.incrementRoundNumber();
            if (model.getRoundNumber() == 40) {
                publisher.dispatch("Time's up, Aliens win!");
                for (PlayerController player : model.getPlayersInGame()) {
                    if (player.getPlayer() instanceof Alien) {
                        removeUser(player.getUser(), true);
                    } else
                        removeUser(player.getUser(), false);
                }

            }

        }

        for (User user : users) {
            try {
                user.writeObject(new EndGameEvent());
            } catch (IOException e) {
                log("Cannot send GameEndEvent to " + user.toString());
                log(e);
                removeDisconnectedUser(user);
            }
        }
        GameManager.notifyGameEnd(this);
    }

    /**
     * Notify to all the player in the game the list of the players in the
     * starting game.
     * 
     * @throws IOException
     */
    protected void notifyGameStartToUsers() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("\nUsers in game:\n");
        for (User u : users) {
            u.writeObject(new NotifyGameStartEvent(map, RuleType.ADVANCED));
            builder.append(u.getName() + "\n");
        }
        publisher.dispatch(builder.toString());

    }

    /**
     * Switch case that, for each action chosen, call the logic of that action.
     * 
     * @param actionChoosen
     * @param currentUser
     */
    protected void executeAction(UserAction actionChoosen, User currentUser) {

        switch (actionChoosen) {

        case MOVE:
            moveLogic(currentUser);
            break;

        case DRAW_CARD:
            drawCardLogic(currentUser);
            break;

        case ATTACK:
            attackLogic(currentUser);
            break;

        case ENDTURN:
            endTurnLogic(currentUser);
            break;

        case USE_CARD:
            useCardLogic(currentUser);
            break;

        default:
            break;
        }

    }

    /**
     * Assigns to each User a Player class, Human or Alien
     */
    protected void assignCharacters() {
        Collections.shuffle(users);
        List<HumanRole> roles = new ArrayList<HumanRole>(
                Arrays.asList(HumanRole.values()));
        Collections.shuffle(roles);
        for (int i = 0; i < users.size(); i++) {
            if (i % 2 == 0) {
                Player player = new Alien(model.getMap().getAlienSector(),
                        users.get(i).getName());
                PlayerController playerController = new PlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            } else {
                Player player = new Human(model.getMap().getHumanSector(),
                        users.get(i).getName(), roles.remove(0));
                PlayerController playerController = new PlayerController(
                        player, controller, users.get(i));
                users.get(i).setPlayer(playerController);
                controller.addPlayer(playerController);
            }
        }
        Collections.shuffle(users);
    }

    /**
     * Add a user to the game. When the second player join the game, a timer is
     * started. When the timer expires, the game start, regardless of the number
     * of players.
     * 
     * @param user
     */
    public void addUser(User user) {
        users.add(user);
        publisher.addSubscriber(user.getSubscriberSocket());
        publisher.dispatch(user + " added");
        if (users.size() == 2) {
            startTimer();
        } else if (users.size() == 8) {
            this.timer.cancel();
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    startGame();
                }
            }, 2000);

        }
    }

    /**
     * start a timer with a delay of 2 minutes. During that period a new user is
     * added to this game. At the end the game start. A new User will have to
     * create another game.
     */
    private void startTimer() {
        publisher.dispatch("Start of Timer");

        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                startGame();

            }
        }, 60 * 1000);

    }

    /**
     * Remove the user from the game, with a different message depending if he's
     * a winner or not.
     * 
     * @param user
     *            User to be removed.
     * @param isWinner
     *            True if the player won the game, false otherwise.
     */
    public void removeUser(User user, boolean isWinner) {
        try {
            if (isWinner) {
                user.writeObject(new MessageEvent(
                        "You win!!! Congratulations! :D"));
            } else
                user.writeObject(new MessageEvent("Game over!"));

            publisher.removeSubscriber(user.getSubscriberSocket());
            synchronized (users) {
                log("remove " + user.getName() + " " + users.remove(user));
                turns.remove(user);
                user.writeObject(new EndGameEvent());
            }
        } catch (IOException e) {
            log("Cannot write GameOver message to user!");
            log(e);
        }

    }

    /**
     * This method is called when Player is disconnected from a game. Disconnect
     * that player form the subscriber and remove it from the game controller.
     * 
     * @param user
     */
    public void removeDisconnectedUser(User user) {
        log("remove " + user.getName() + " " + users.remove(user));
        turns.remove(user);
        controller.removeDisconnectedPlayer(user.getPlayerController());
        publisher.removeSubscriber(user.getSubscriberSocket());
    }

    private static boolean containsSubArray(List<int[]> j, int[] sub) {
        for (int[] arr : j) {
            if (Arrays.equals(arr, sub)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the use card choice to the possible actions only if the Player is a
     * Human.
     * 
     * @param currentUser
     * @param possibleActions
     */
    protected void addUseCard(User currentUser, List<UserAction> possibleActions) {
        if (currentUser.getPlayerController().hasPlayableCards()
                && (currentUser.getPlayerController().getPlayer() instanceof Human)) {
            possibleActions.add(UserAction.USE_CARD);
        }
    }

    /**
     * If a player is an Alien, add the "Attack" choice to the possible actions.
     * 
     * @param currentUser
     * @param possibleActions
     */
    protected void addAttack(User currentUser, List<UserAction> possibleActions) {
        if (currentUser.getPlayerController().getPlayer() instanceof Alien) {
            possibleActions.add(UserAction.ATTACK);
        }
    }

    /**
     * Description of what happens before and after a Player moves from one
     * position to another.
     * 
     * @param currentUser
     */
    protected void moveLogic(User currentUser) {

        List<Sector> possibleDestinations = getPossibleSectors(currentUser);
        List<int[]> possibleCoordinates = new ArrayList<>();

        for (int i = 0; i < possibleDestinations.size(); i++) {
            Sector s = possibleDestinations.get(i);
            int[] array = new int[2];
            array[0] = s.getX();
            array[1] = s.getY();
            possibleCoordinates.add(array);
        }

        int[] response = null;
        // continue asking for coordinate till they are correct

        try {
            do {
                currentUser.writeObject(new AskForPositionEvent(
                        "You can move in: ",
                        (ArrayList<int[]>) possibleCoordinates));
                response = (int[]) currentUser.readObject();
                log(response[0] + " " + response[1]);

            } while (!containsSubArray(possibleCoordinates, response));

            Sector destination = model.getMap().getSector(response[0],
                    response[1]);
            currentUser.getPlayerController().movePlayer(destination);

            currentUser.writeObject(new MessageEvent("You moved in "
                    + currentUser.getPlayerController().getPlayer()
                            .getPosition()));

            if (movedInDangerousSector(currentUser)) {
                model.setGameState(GameState.BEFORE_DRAW);
            } else
                try {
                    Sector position = currentUser.getPlayerController()
                            .getPlayer().getPosition();
                    if (position instanceof EscapeHatchSector
                            && currentUser.getPlayerController().getPlayer() instanceof Human) {
                        if (((EscapeHatchSector) position).getType().equals(
                                EscapeHatchType.RED)) {
                            currentUser
                                    .writeObject(new MessageEvent(
                                            "This Escape Hatch is damaged! You cannot escape!"));
                        }
                        if (currentUser
                                .getPlayerController()
                                .getController()
                                .checkHumansWinningConditions(
                                        currentUser.getPlayerController())) {
                            removeUser(currentUser, true);
                            if (controller.checkAliensWinningConditions()) {
                                publisher
                                        .dispatch("There are no Humans left in the spaceship, Alien squad lost the game!");
                                for (PlayerController player : model
                                        .getPlayersInGame()) {
                                    if (player.getPlayer() instanceof Alien) {
                                        removeUser(player.getUser(), false);
                                    }
                                }
                                model.setGameState(GameState.GAME_END);
                            } else
                                model.setGameState(GameState.ENDTURN);
                        }
                    }

                } catch (EmptyDeckException e) {
                    // never happening
                }

        } catch (IOException e2) {
            log("Cannot communicate with " + currentUser);
            log(e2);
        } catch (ClassNotFoundException e) {
            log("Cannot read informations from user");
            log(e);
        }

    }

    /**
     * Check if the destination is a Dangerous sector.
     * 
     * @param currentUser
     * @return
     */
    protected boolean movedInDangerousSector(User currentUser) {
        if (!(currentUser.getPlayerController().getPlayer().isUnderSedatives())
                && currentUser.getPlayerController().getPlayer().getPosition() instanceof DangerousSector) {
            return true;
        }
        return false;
    }

    /**
     * Draw a sector card, then check what kind of sector card is the drawn
     * card. If the card is a Silence Card, the publisher dispatch a silence
     * message; if Noise in Any Sector the publisher dispatch the current
     * location of the player; if Noise in Any Sector the publisher dispatch a
     * message with a fictional location chosen by the player.
     * 
     * @param currentUser
     */
    protected void drawCardLogic(User currentUser) {
        DangerousSectorCard card = null;
        try {
            card = controller.getDeckController().drawSectorCard();
            // SILENCE CARD LOGIC
            if (card instanceof SilenceCard) {
                currentUser.writeObject(new SilenceEvent());
                currentUser.readObject();
                publisher.dispatch(currentUser + " declares Silence!");
                // NOISE IN YOUR SECTOR LOGIC
            } else if (card instanceof NoiseInYourSector) {
                currentUser.writeObject(new NoiseInYourSectorEvent());
                currentUser.readObject();
                publisher.dispatch(currentUser
                        + ": noise in "
                        + currentUser.getPlayerController().getPlayer()
                                .getPosition());
                // NOISE IN ANY SECTOR LOGIC
            } else {
                Sector position = null;
                do {
                    currentUser
                            .writeObject(new AskForPositionEvent(
                                    "You draw a Noise In Any Sector card. Please insert coordinates: (A 1)",
                                    null));
                    int[] coordinates = (int[]) currentUser.readObject();
                    log(coordinates[0] + " " + coordinates[1]);
                    position = model.getMap().getSector(coordinates[0],
                            coordinates[1]);
                    log(position);
                } while (position == null || position instanceof VoidSector
                        || position instanceof HumanSector
                        || position instanceof AlienSector);
                publisher.dispatch(currentUser + ": noise in " + position);
            }
            /**
             * check if user can draw an object card, and do it
             */
            drawObjectCardLogic(currentUser, card);

        } catch (EmptyDeckException e) {
            // non accadrÃƒÂ  mai
        } catch (IOException e) {
            log("cannot write MessageEvent!");
            log(e);
        } catch (ClassNotFoundException e) {
            log("cannot write MessageEvent!");
            log(e);
        }
        controller.getDeckController().discardSectorCard(card);
        model.setGameState(GameState.AFTER_ATTACK);
    }

    /**
     * First, call attack method in Player Controller, then checks if aliens
     * have won or if there's only one human left.
     * 
     * @param currentUser
     */
    protected void attackLogic(User currentUser) {
        currentUser.getPlayerController().attack();

        if (controller.checkAliensWinningConditions()) {
            publisher.dispatch("The Alien Squad has won!!! Om nom nom");
            for (PlayerController player : model.getPlayersInGame()) {
                if (player.getPlayer() instanceof Alien) {
                    removeUser(player.getUser(), true);
                }
            }
            model.setGameState(GameState.GAME_END);
        }

        else if (model.getPlayersInGame().size() == 1
                && currentUser.getPlayerController().getPlayer() instanceof Human) {
            publisher
                    .dispatch("There is only one human lost in the spaceship! He's the winner!");
            removeUser(currentUser, true);
            model.setGameState(GameState.GAME_END);
        }

        else {
            model.setGameState(GameState.AFTER_ATTACK);
        }

    }

    /**
     * Show the list of all the card held by the player. When a card is chosen,
     * this method calls the relative action.
     * 
     * @param currentUser
     */
    protected void useCardLogic(User currentUser) {
        List<ObjectCard> objects = currentUser.getPlayerController()
                .getPlayer().getObjects();
        List<ObjectCardType> types = new ArrayList<ObjectCardType>();
        for (int i = 0; i < objects.size(); i++) {
            if (!(objects.get(i).getType() == ObjectCardType.DEFENSE))
                types.add(objects.get(i).getType());
        }
        ObjectCardType[] arrayTypes = types.toArray(new ObjectCardType[types
                .size()]);
        log(currentUser + " can use: " + Arrays.toString(arrayTypes));

        try {
            currentUser.writeObject(new AskForCardToUseEvent(types
                    .toArray(new ObjectCardType[types.size()])));
            ObjectCardType cardChoosen = (ObjectCardType) currentUser
                    .readObject();
            Action action;
            publisher.dispatch("Human " + currentUser.getName() + " uses "
                    + cardChoosen + " card!");
            switch (cardChoosen) {
            case ADRENALINE:
                action = new AdrenalineAction();
                action.executeAction(currentUser.getPlayerController());
                break;
            case SEDATIVES:
                action = new SedativesAction();
                action.executeAction(currentUser.getPlayerController());
                break;
            case TELEPORT:
                action = new TeleportAction();
                action.executeAction(currentUser.getPlayerController());
                break;
            case SPOTLIGHT:
                action = new SpotlightAction();
                spotlightLogic(action, currentUser);
                break;
            case ATTACK:
                attackLogic(currentUser);
                break;
            default:
                break;
            }

        } catch (IOException | ClassNotFoundException e) {
            log("Cannot write or read object to/from user");
            log(e);
        }
    }

    /**
     * When a Player uses a Spotlight card, this method asks in which sector he
     * wants to use the cards, then call SpotlightAction.
     * 
     * @param action
     * @param currentUser
     */
    protected void spotlightLogic(Action action, User currentUser) {
        try {
            Sector position = null;
            int[] coordinates = null;
            do {
                currentUser.writeObject(new AskForPositionEvent(
                        "Insert valid coordinates for Spotlight (A 1):", null));
                coordinates = (int[]) currentUser.readObject();
                position = model.getMap().getSector(coordinates[0],
                        coordinates[1]);
            } while (position == null || (position instanceof VoidSector));
            ((SpotlightAction) action).setCoordinates(coordinates[0],
                    coordinates[1]);
            action.executeAction(currentUser.getPlayerController());
        } catch (IOException | ClassNotFoundException e) {
            log("Cannot write or read object to/from user");
            log(e);
        }
    }

    /**
     * Logic of a card's drawing. When a player must draw an ObjectCard, this //
     * * method draw the card from the ObjectCard deck and checks if the player
     * already has 3 Object Cards, if so asks the player which card he wants to
     * discard.
     * 
     * @param currentUser
     * @param cardDrawn
     */
    protected void drawObjectCardLogic(User currentUser,
            DangerousSectorCard cardDrawn) {
        if (cardDrawn.hasObject()) {
            try {
                ObjectCard objectCard = controller.getDeckController()
                        .drawObjectCard();
                Player player = currentUser.getPlayerController().getPlayer();
                player.addObject(objectCard);
                currentUser.writeObject(new MessageEvent(
                        "You draw an object card: " + objectCard));
                if (player.getObjects().size() > player.getMaxObjects()) {
                    List<ObjectCard> objects = currentUser
                            .getPlayerController().getPlayer().getObjects();
                    ObjectCardType[] types = new ObjectCardType[objects.size()];
                    for (int i = 0; i < objects.size(); i++) {
                        types[i] = objects.get(i).getType();
                    }
                    currentUser.writeObject(new AskToDiscardEvent(types));
                    ObjectCardType cardToDiscard = (ObjectCardType) currentUser
                            .readObject();
                    player.removeObject(new ObjectCard(cardToDiscard));
                    controller.getDeckController().discardObjectCard(
                            new ObjectCard(cardToDiscard));
                }
            } catch (EmptyDeckException e) {
                // non accadrÃ  mai
            } catch (IOException e) {
                log("cannot write MessageEvent!");
                log(e);
            } catch (ClassNotFoundException e) {
                log("cannot write MessageEvent!");
                log(e);
            }
        }
    }

    /**
     * Print "End Turn" and set the GameState to endturn.
     * 
     * @param currentUser
     */
    protected void endTurnLogic(User currentUser) {
        try {
            currentUser.writeObject(new MessageEvent("End turn."));
        } catch (IOException e) {
            log("Cannot write message");
            log(e);
        }
        model.setGameState(GameState.ENDTURN);

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((controller == null) ? 0 : controller.hashCode());
        result = prime * result + ((model == null) ? 0 : model.hashCode());
        result = prime * result + ((users == null) ? 0 : users.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }

    public void log(String message) {
        LOGGER.print("GAME " + currentGame + ": " + new Date() + " - "
                + message);
    }

    public void log(Object object) {
        LOGGER.print("GAME " + currentGame + ": " + new Date() + " - "
                + object.toString());
    }

    protected void roundStartMessage(User currentUser) throws IOException {
        currentUser.writeObject(new MessageEvent(currentUser + " "
                + currentUser.getPlayerController().getPlayer().getPosition()));
    }

    protected List<Sector> getPossibleSectors(User currentUser) {
        return new ArrayList<Sector>(currentUser.getPlayerController()
                .getPossibleDestinations());
    }

    @Override
    public String toString() {
        return "GAME " + currentGame;
    }
}
