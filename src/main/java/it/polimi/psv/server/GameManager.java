package it.polimi.psv.server;

import it.polimi.psv.view.logger.CLILogger;
import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Server that hosts and manage the games.
 * 
 * @author alexvago
 *
 */
public class GameManager {

    private static final int PORT = 29999;
    private static final int PUBLISHER_PORT = 29998;
    private static final Logger LOGGER = new CLILogger();

    private static List<GameControllerThread> waitingGames;
    private static List<GameControllerThread> runningGames;

    public GameManager() {
        waitingGames = new ArrayList<GameControllerThread>();
        runningGames = new ArrayList<GameControllerThread>();
    }

    /**
     * Start the server, with his sockets (Client and Subscriber), accept
     * connections from the client and call AddUserThread.
     * 
     * @throws ClassNotFoundException
     */
    public void startServer() throws ClassNotFoundException {

        /**
         * ExecutorService represents an asynchronous execution mechanism which
         * is capable of executing tasks in the background.
         * Executors.newCachedThreadPool() Creates a thread pool that creates
         * new threads as needed, but will reuse previously constructed threads
         * when they are available.
         */
        ExecutorService executor = Executors.newCachedThreadPool();

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            LOGGER.print("GameManager ready on Port: " + PORT);
            ServerSocket publisherSocket = new ServerSocket(PUBLISHER_PORT);
            while (true) {

                try {

                    Socket socket = serverSocket.accept();
                    Socket subscriberSocket = publisherSocket.accept();
                    LOGGER.print("new Client connected.");

                    executor.submit(new AddUserThread(socket, subscriberSocket,
                            executor));

                } catch (IOException e) {
                    break;
                }
            }
            // shutdown the executor
            executor.shutdown();

            // closes the ServerSocket
            serverSocket.close();
            publisherSocket.close();
        } catch (IOException e) {
            LOGGER.print("Impossibile lanciare il server!");
            LOGGER.print(e);
        }
    }

    /**
     * Notification of when a new Game starts, removing that game from
     * waitingGames.
     * 
     * @param game
     *            GameControllerThread of the starting game.
     */
    public static void notifyGameStart(GameControllerThread game) {
        if (waitingGames.contains(game)) {
            waitingGames.remove(game);
            runningGames.add(game);
            LOGGER.print(game + " started.");
        } else {
            LOGGER.print(game.toString() + " not in waiting list");
        }
    }

    /**
     * Removes the game passed as parameter from the running game list.
     * 
     * @param game
     */
    public static void notifyGameEnd(GameControllerThread game) {
        runningGames.remove(game);
        LOGGER.print(game + " ended.");

    }

    public static List<GameControllerThread> getWaitingGames() {
        return waitingGames;
    }

    public static List<GameControllerThread> getRunningGames() {
        return runningGames;
    }

    /**
     * Creates a server and starts it.
     * 
     * @param args
     */
    public static void main(String[] args) {
        GameManager server = new GameManager();
        try {
            server.startServer();
        } catch (ClassNotFoundException e) {
            LOGGER.print("Cannot start the server!");
            LOGGER.print(e);
        }
    }

    /**
     * Add a game to list of current Games running/waiting for start. Should be
     * callable only there isn't other games waiting for start.
     * 
     * @param game
     *            The GameControllerThread of the new game.
     */
    public static void addGame(GameControllerThread game) {
        waitingGames.add(game);

    }

    /**
     * Add a user to a starting game.
     * 
     * @param user
     *            The user who decide the rules.
     */
    public static void addUserToGame(User user) {
        waitingGames.get(0).addUser(user);
    }

}
