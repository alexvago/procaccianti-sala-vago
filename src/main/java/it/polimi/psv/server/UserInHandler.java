package it.polimi.psv.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class UserInHandler implements Runnable {

    private final ObjectInputStream input;

    public UserInHandler(Socket socket) throws IOException {
        this.input = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {

    }

    /**
     * reads object from stream
     * 
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public Object readObject() throws ClassNotFoundException, IOException {
        return input.readObject();
    }

}
