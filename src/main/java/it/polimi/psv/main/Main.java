package it.polimi.psv.main;

import it.polimi.psv.client.Client;
import it.polimi.psv.client.GUIClient;
import it.polimi.psv.view.logger.CLILogger;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    private Main() {

    }

    public static void main(String[] args) {

        Scanner stdin = new Scanner(System.in);
        String choice;
        do {
            System.out.println("CLI [0] / GUI[1]: ");
            choice = stdin.nextLine();

        } while (!choice.matches("[01]"));
        if (Integer.valueOf(choice) == 0) {
            System.out.println("Insert your name: ");
            String username = stdin.nextLine();
            try {
                Client client = new Client(username, new CLILogger());
                // runs the Client
                client.startClient();
            } catch (IOException e) {
                System.out.println("Cannot start CLI client");
                System.out.println(e);
            }
        } else {
            try {
                new GUIClient().startClient();
            } catch (IOException e) {
                System.out.println("Cannot start GUI client");
                System.out.println(e);
            }
        }
        stdin.close();
        

    }

}
