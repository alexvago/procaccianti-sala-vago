package it.polimi.psv.view.logger;

public interface Logger {

    public void print(String message);

    public void print(Object obj);
}
