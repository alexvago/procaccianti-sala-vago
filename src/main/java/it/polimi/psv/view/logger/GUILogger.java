package it.polimi.psv.view.logger;

import javax.swing.JTextArea;

/**
 * Implements the logger when a GUI is used, prints strings or objects.
 */
public class GUILogger implements Logger {

    private JTextArea console;

    public GUILogger(JTextArea console) {
        this.console = console;
    }

    @Override
    public void print(String message) {
        console.append(message + "\n");

    }

    @Override
    public void print(Object obj) {
        console.append(obj.toString() + "\n");

    }

}
