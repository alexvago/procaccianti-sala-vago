package it.polimi.psv.view.logger;

/**
 * Implements the logger when a CLI is used, prints strings or objects.
 */
public class CLILogger implements Logger {

    @Override
    public void print(String message) {
        System.out.println(message);

    }

    @Override
    public void print(Object obj) {
        System.out.println(obj.toString());

    }

}
