package it.polimi.psv.model.sector;

/**
 * Coordinate, location of a sector in Carthesian Coordinates.
 */
public class Coordinate {

    protected final int x;
    protected final int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
