package it.polimi.psv.model.sector;

/**
 * Enumeration of all types of Escape Hatch.
 */
public enum EscapeHatchType {

    UNDEFINED, GREEN, RED;
}
