package it.polimi.psv.model.sector;

/**
 * Secure Sector, when Players can't draw a Card.
 */
public class SecureSector extends Sector {

    public SecureSector(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {

        return "SecureSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }
}
