package it.polimi.psv.model.sector;

/**
 * VoidSector represents the empty sectors in the map
 *
 * @author alexvago
 *
 */
public class VoidSector extends Sector {

    public VoidSector(int x, int y) {
        super(x, y);

    }

    @Override
    public String toString() {
        return "VoidSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }

}
