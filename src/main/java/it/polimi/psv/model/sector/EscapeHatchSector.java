package it.polimi.psv.model.sector;

/**
 * Escape Hatch Sector, with type set to default on Undefined and changed when a
 * Player land in a Escape Hatch Sector.
 */
public class EscapeHatchSector extends Sector {

    private EscapeHatchType type;

    public EscapeHatchSector(int x, int y) {
        super(x, y);
        this.type = EscapeHatchType.UNDEFINED;
    }

    public void setType(EscapeHatchType type) {
        this.type = type;
    }

    public EscapeHatchType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "EscapeHatchSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }

}
