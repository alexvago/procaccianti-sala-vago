package it.polimi.psv.model.sector;

/**
 * Dangerous Sector Creator, including toString.
 */
public class DangerousSector extends Sector {

    public DangerousSector(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "DangerousSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }
}
