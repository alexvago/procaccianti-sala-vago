package it.polimi.psv.model.sector;

import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.util.HashCode;

import java.util.HashSet;
import java.util.Set;

/**
 * Sector Class, including methods to add/remove Players from a sector and know
 * how many and which Players are currently in that Sector.
 */
public class Sector extends Coordinate {
    /**
     * Set of players in this sectors
     */
    private Set<PlayerController> playersInSector;

    public Sector(int x, int y) {
        super(x, y);
        this.playersInSector = new HashSet<PlayerController>();
    }

    public Set<PlayerController> getPlayersInSector() {
        return playersInSector;
    }

    /**
     * Add a player in a sector.
     * 
     * @param Player
     */
    public void addPlayer(PlayerController player) {
        playersInSector.add(player);
    }

    /**
     * Remove a player in a sector.
     * 
     * @param Player
     */
    public void removePlayer(PlayerController p) {
        playersInSector.remove(p);
    }

    @Override
    public String toString() {
        return "Sector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }

    @Override
    public int hashCode() {
        return HashCode.hashCode(x, y);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Coordinate other = (Coordinate) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

}
