package it.polimi.psv.model.sector;

/**
 * Alien Sector Creator, with his toString. Starting position for Alien Players.
 */
public class AlienSector extends Sector {

    public AlienSector(int x, int y) {
        super(x, y);

    }

    @Override
    public String toString() {
        return "AlienSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }
}
