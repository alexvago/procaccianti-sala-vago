package it.polimi.psv.model.sector;

/**
 * Human Sector with his coordinates. Starting sector for Human Players.
 */
public class HumanSector extends Sector {

    public HumanSector(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "HumanSector " + (char) (x + 65)
                + String.format("%02d", (x / 2) + y + 1);
    }

}
