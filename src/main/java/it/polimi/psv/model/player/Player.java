package it.polimi.psv.model.player;

import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.sector.Sector;

import java.util.ArrayList;
import java.util.List;

/**
 * Describe a generic Player, which can be an Alien or a Human.
 */

public abstract class Player {

    protected final String name;
    private final List<ObjectCard> objects;
    protected Sector position;
    protected int speed;
    private boolean underSedatives = false;
    private boolean identityRevealed = false;
    private boolean notMoved = false;
    private boolean captainPassedOnFirstDangerous = false;
    private int maxObjects = 3;
    private int pilotsNumberOfTeleports = 0;

    /**
     * Constructor of a Player.
     *
     * @param position
     *            ,name The position where the player will be added and his name
     * 
     */

    public Player(Sector position, String name) {
        this.objects = new ArrayList<ObjectCard>();
        this.position = position;
        this.name = name;
    }

    /**
     * Get the position of the current player.
     *
     * @return the sector in which is located the Player
     *
     */

    public Sector getPosition() {
        return this.position;
    }

    /**
     * Add an ObjectCard into the player's objects list with the type specified
     * in the parameter.
     * 
     * @param type
     *            The type of the card to be added.
     */

    public void addObject(ObjectCard card) {
        objects.add(card);
    }

    /**
     * Removes from the player's objects list the object card with the type
     * specified in the parameter.
     * 
     * @param type
     *            The type of the card to be removed.
     * @return The card removed.
     */

    public ObjectCard removeObject(ObjectCard card) {
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i).equals(card)) {
                return objects.remove(i);
            }
        }
        return null;

    }

    /**
     * Getter of the Object cards of the player.
     *
     * 
     * @return the list of the Objects Cards own by the player
     *
     */

    public List<ObjectCard> getObjects() {
        return objects;
    }

    /**
     * Move the player into the destination sector.
     *
     * @param destination
     *            The destination's sector
     */

    public void setPosition(Sector destination) {
        this.position = destination;
    }

    /**
     * Retrieve the player's movement speed.
     * 
     * @return The player's current speed.
     */

    public int getSpeed() {
        return this.speed;
    }

    /**
     * Increments the Player's speed by one.
     * 
     */

    public void incrementSpeed() {
        this.speed++;
    }

    /**
     * Decrements the Player's speed by one.
     */

    public void decrementSpeed() {
        this.speed--;
    }

    /**
     * Specifies if the Player has used the Sedatives Card in the current turn.
     * 
     * @return True if the player is under Sedatives, false otherwise
     */

    public boolean isUnderSedatives() {
        return underSedatives;
    }

    /**
     * Change the state of the Player if he uses the Sedatives Object Card.
     * 
     * @param underSedatives
     *            Boolean related to the current state of the Player.
     * 
     */

    public void setUnderSedatives(boolean underSedatives) {
        this.underSedatives = underSedatives;
    }

    /**
     * Retrieve the max number of ObjectCard a player can have. Should be 4 for
     * Soldier and 3 for everyone else.
     * 
     * @return The value of maxObjects.
     */

    public int getMaxObjects() {
        return maxObjects;
    }

    /**
     * Change the max value of ObjectCard a player can have.
     * 
     * @param maxObjects
     *            The actual size of maxObjects int.
     */

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    /**
     * Show the name of the Player.
     *
     * @return The name of the Player.
     */

    public String getName() {
        return name;
    }

    public int getPilotsNumberOfTeleports() {
        return pilotsNumberOfTeleports;
    }

    public void incrementPilotsNumberOfTeleports() {
        pilotsNumberOfTeleports++;
    }

    public void resetPilotsNumberOfTeleports() {
        pilotsNumberOfTeleports = 0;
    }

    public boolean isIdentityRevealed() {
        return identityRevealed;
    }

    public void setIdentityRevealed(boolean identityRevealed) {
        this.identityRevealed = identityRevealed;
    }

    public boolean isNotMoved() {
        return notMoved;
    }

    public void setNotMoved(boolean notMoved) {
        this.notMoved = notMoved;
    }

    public boolean isCaptainPassedOnFirstDangerous() {
        return captainPassedOnFirstDangerous;
    }

    public void setCaptainPassedOnFirstDangerous(
            boolean captainPassedOnFirstDangerous) {
        this.captainPassedOnFirstDangerous = captainPassedOnFirstDangerous;
    }

    @Override
    public String toString() {
        return "Player [name=" + name + ", position=" + position + "]";
    }

}
