package it.polimi.psv.model.player;

import it.polimi.psv.model.sector.Sector;

/**
 * Extension of Player, this class describe an Alien.
 * 
 */
public class Alien extends Player {

    /**
     * Constructor of an Alien Player, extension of Player, with default speed
     * set at 2.
     * 
     * @param position
     *            , name The Alien's initial position and his name.
     */

    public Alien(Sector position, String name) {
        super(position, name);
        this.speed = 2;
    }

    @Override
    public String toString() {
        return "Alien [name=" + name + ", position=" + position + "]";
    }

}
