package it.polimi.psv.model.player;

import it.polimi.psv.model.sector.Sector;

/**
 * Extension of a Player, has one more attribute, Role, used in Old School Mode
 * to enable Human's specific abilities.
 */

public class Human extends Player {

    private final HumanRole role;

    /**
     * Constructor of Human, extension of Player, with default speed set at 1.
     *
     * @param position
     *            , name, role The player's position, his name and role.
     *
     */

    public Human(Sector position, String name, HumanRole role) {
        super(position, name);
        this.speed = 1;
        this.role = role;
    }

    /**
     * Show the role of the Player, if he's a Human.
     * 
     * @return Player's role.
     */

    public HumanRole getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Human [name=" + name + ", position=" + position + "]";
    }

}
