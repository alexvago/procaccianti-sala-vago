package it.polimi.psv.model.player;

/**
 * List of all possible Human roles.
 * 
 */

public enum HumanRole {
    CAPTAIN, PILOT, PSYCHOLOGIST, SOLDIER, EXECUTIVEOFFICER, COPILOT, ENGINEER, MEDIC;
}
