package it.polimi.psv.model.state;

import it.polimi.psv.controller.map.MapCreator;
import it.polimi.psv.controller.player.PlayerController;
import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.decks.DangerousSectorDeckCreator;
import it.polimi.psv.model.decks.Deck;
import it.polimi.psv.model.decks.EscapeHatchDeckCreator;
import it.polimi.psv.model.decks.ObjectDeckCreator;
import it.polimi.psv.model.map.GameMap;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.model.player.Player;

import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

/**
 * GameModel, manage the GameState, contains references to player, decks, map in
 * a Game. Allows to change the round Number.
 */
public class GameModel extends Observable {

    private final Set<PlayerController> playersInGame;
    private final Set<PlayerController> winners;
    private final Deck<DangerousSectorCard> sectorDeck;
    private final Deck<EscapeHatchCard> escapeHatchDeck;
    private final Deck<ObjectCard> objectDeck;
    private final GameMap map;
    private GameState gameState;
    private int roundNumber = 1;
    private Player currentPlayer;

    public GameModel(MapType mapType) {
        this.playersInGame = new HashSet<PlayerController>();
        this.winners = new HashSet<PlayerController>();
        this.map = MapCreator.createMapFromFile(mapType.getFile());
        this.gameState = GameState.WAITING_FOR_PLAYERS;
        this.sectorDeck = new DangerousSectorDeckCreator().createDeck();
        this.escapeHatchDeck = new EscapeHatchDeckCreator().createDeck();
        this.objectDeck = new ObjectDeckCreator().createDeck();
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
        this.setChanged();
        this.notifyObservers(gameState);
    }

    /**
     * When all player complete their Turn, increment by one the Round number.
     * If the round number is 40, the game is stopped.
     */
    public void incrementRoundNumber() {
        roundNumber++;
        if (roundNumber == 40) {
            setGameState(GameState.GAME_END);
        }
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Set<PlayerController> getPlayersInGame() {
        return playersInGame;
    }

    public void addPlayer(PlayerController player) {
        playersInGame.add(player);
    }

    /**
     * Remove a killed Player from the GameController, then remove him from the
     * Map.
     * 
     * @param p
     *            The player killed and to be removed.
     */
    public void removePlayer(PlayerController p) {
        playersInGame.remove(p);
        p.getPlayer().getPosition().removePlayer(p);
    }

    public Deck<EscapeHatchCard> getEscapeHatchDeck() {
        return escapeHatchDeck;
    }

    public Set<PlayerController> getWinners() {
        return winners;
    }

    /**
     * Add a Player into the set of winning PlayerController if his winning
     * conditions are met.
     * 
     * @param player
     *            One of the winners.
     */
    public void addWinner(PlayerController player) {
        winners.add(player);
    }

    public Deck<DangerousSectorCard> getSectorDeck() {
        return sectorDeck;
    }

    public Deck<ObjectCard> getObjectDeck() {
        return objectDeck;
    }

    public GameMap getMap() {
        return map;
    }

    public GameState getGameState() {
        return gameState;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

}
