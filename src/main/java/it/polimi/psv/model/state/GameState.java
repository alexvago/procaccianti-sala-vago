package it.polimi.psv.model.state;

/**
 * Enumeration of all the possible Game/Turn State.
 */
public enum GameState {

    WAITING_FOR_PLAYERS, GAME_END, BEFORE_MOVE, AFTER_MOVE, ENDTURN, AFTER_ATTACK, BEFORE_DRAW;

}
