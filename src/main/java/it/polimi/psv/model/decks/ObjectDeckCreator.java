package it.polimi.psv.model.decks;

import it.polimi.psv.model.cards.ObjectCard;
import it.polimi.psv.model.cards.ObjectCardType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Create the Object deck with 12 cards.
 */
public class ObjectDeckCreator implements DeckCreator {

    @Override
    public Deck<ObjectCard> createDeck() {
        List<ObjectCard> cards = new ArrayList<ObjectCard>();

        cards.add(new ObjectCard(ObjectCardType.ATTACK));
        cards.add(new ObjectCard(ObjectCardType.ATTACK));

        cards.add(new ObjectCard(ObjectCardType.TELEPORT));
        cards.add(new ObjectCard(ObjectCardType.TELEPORT));

        cards.add(new ObjectCard(ObjectCardType.ADRENALINE));
        cards.add(new ObjectCard(ObjectCardType.ADRENALINE));

        cards.add(new ObjectCard(ObjectCardType.SEDATIVES));
        cards.add(new ObjectCard(ObjectCardType.SEDATIVES));
        cards.add(new ObjectCard(ObjectCardType.SEDATIVES));

        cards.add(new ObjectCard(ObjectCardType.SPOTLIGHT));
        cards.add(new ObjectCard(ObjectCardType.SPOTLIGHT));

        cards.add(new ObjectCard(ObjectCardType.DEFENSE));

        Collections.shuffle(cards);

        return new Deck<ObjectCard>(cards);

    }

}
