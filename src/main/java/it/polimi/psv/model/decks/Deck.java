package it.polimi.psv.model.decks;

import it.polimi.psv.model.cards.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Methods associated to the management of a Deck, including
 * shuffle,draw,discard and the size of the deck.
 */
public class Deck<C extends Card> {

    private final List<C> gameDeck;
    private final List<C> graveyard;

    public Deck(List<C> deck) {
        this.gameDeck = new ArrayList<C>(deck);
        this.graveyard = new ArrayList<C>();

    }

    /**
     * Method for drawing a card from the game deck. Must call the method
     * {@link canDraw()} before calling this.
     *
     * @return C the card drawn
     *
     */
    public C draw() throws EmptyDeckException {
        if (gameDeck.isEmpty()) {
            throw new EmptyDeckException();
        }
        C cardToReturn = gameDeck.remove(0);
        if (gameDeck.isEmpty()) {
            shuffle();
        }
        return cardToReturn;
    }

    /**
     * Discard a card, that is add it to the graveyard
     * 
     * @param card
     *            the card to be discarded
     */
    public void discard(C card) {
        graveyard.add(card);
    }

    public boolean isEmpty() {
        return gameDeck.isEmpty();
    }

    public int size() {
        return gameDeck.size();
    }

    /**
     * reshuffles the graveyard and refills the gameDeck
     */
    private void shuffle() {
        Collections.shuffle(graveyard);
        gameDeck.clear();
        gameDeck.addAll(graveyard);
        graveyard.clear();
    }

    public List<C> getGameDeck() {
        return gameDeck;
    }

    public List<C> getGraveyard() {
        return graveyard;
    }

    @Override
    public String toString() {
        return "Deck [gameDeck=" + gameDeck + "]";
    }

}
