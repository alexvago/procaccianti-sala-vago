package it.polimi.psv.model.decks;

import it.polimi.psv.model.cards.EscapeHatchCard;
import it.polimi.psv.model.sector.EscapeHatchType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Creates the Escape Hatch Deck, with 3 green Hatch cards and 3 Red hatch
 * cards.
 */
public class EscapeHatchDeckCreator implements DeckCreator {

    @Override
    public Deck<EscapeHatchCard> createDeck() {
        List<EscapeHatchCard> cards = new ArrayList<EscapeHatchCard>();

        for (int i = 0; i < 3; i++) {
            cards.add(new EscapeHatchCard(EscapeHatchType.GREEN));
            cards.add(new EscapeHatchCard(EscapeHatchType.RED));
        }

        Collections.shuffle(cards);

        return new Deck<EscapeHatchCard>(cards);
    }

}
