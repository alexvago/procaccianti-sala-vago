package it.polimi.psv.model.decks;

public interface DeckCreator {

    public abstract Deck<?> createDeck();
}
