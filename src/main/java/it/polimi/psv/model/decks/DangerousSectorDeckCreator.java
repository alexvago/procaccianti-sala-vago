package it.polimi.psv.model.decks;

import it.polimi.psv.model.cards.DangerousSectorCard;
import it.polimi.psv.model.cards.NoiseInAnySector;
import it.polimi.psv.model.cards.NoiseInYourSector;
import it.polimi.psv.model.cards.SilenceCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Creates the Dangerous Sector Deck, the cards drawn when a Player lands of a
 * Dangerous sector.
 */
public class DangerousSectorDeckCreator implements DeckCreator {

    @Override
    public Deck<DangerousSectorCard> createDeck() {
        List<DangerousSectorCard> cards = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cards.add(new SilenceCard(false));
        }
        for (int i = 0; i < 10; i++) {
            if (i < 4)
                cards.add(new NoiseInAnySector(true));
            else
                cards.add(new NoiseInAnySector(false));
        }
        for (int i = 0; i < 10; i++) {
            if (i < 4)
                cards.add(new NoiseInYourSector(true));
            else
                cards.add(new NoiseInYourSector(false));
        }

        Collections.shuffle(cards);

        return new Deck<DangerousSectorCard>(cards);
    }

}
