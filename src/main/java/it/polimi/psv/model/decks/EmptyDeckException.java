package it.polimi.psv.model.decks;

/**
 * What to do when a Deck is empty.
 */
public class EmptyDeckException extends Exception {

    /**
	 * 
	 */
    private static final long serialVersionUID = 6220437985615892704L;

    public EmptyDeckException() {
        super("The deck is empty!");
    }
}
