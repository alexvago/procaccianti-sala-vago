package it.polimi.psv.model.cards;

/**
 * Card class and it's generic methods.
 */

public class ObjectCard extends Card {

    private final ObjectCardType type;

    /**
     * Constructor of an Object Card.
     *
     * @param type
     *            the type of the Object Card.
     */

    public ObjectCard(ObjectCardType type) {
        this.type = type;
    }

    /**
     * Getter of the type of an Object Card.
     *
     * @return the type of an Object Card.
     *
     */

    public ObjectCardType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ObjectCard [type=" + type + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObjectCard other = (ObjectCard) obj;
        if (type != other.type)
            return false;
        return true;
    }

}
