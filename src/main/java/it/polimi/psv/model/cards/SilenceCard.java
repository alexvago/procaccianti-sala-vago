package it.polimi.psv.model.cards;

public class SilenceCard extends DangerousSectorCard {

    /**
     * Constructor of a Silence Card.
     *
     * @param hasObject
     *            In the silence card this boolean is always false.
     */
    public SilenceCard(boolean hasObject) {
        super(hasObject);
    }

}
