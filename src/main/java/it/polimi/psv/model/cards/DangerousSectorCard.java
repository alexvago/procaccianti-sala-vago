package it.polimi.psv.model.cards;

/**
 * Generic Dangerous Card, father of all the types of Dangerous cards.
 */
public class DangerousSectorCard extends Card {

    private final boolean hasObject;

    /**
     * Constructor of a Dangerous Sector card.
     *
     * @param hasObject
     *            Boolean, true if the Player must draw a Sector Card after
     *            drawing this card, false otherwise.
     */
    public DangerousSectorCard(boolean hasObject) {

        this.hasObject = hasObject;
    }

    /**
     * Getter of the hasObject attribute of a Dangerous Sector Card.
     *
     * @return true if the cards has the attribute, false otherwise.
     *
     */

    public boolean hasObject() {
        return hasObject;
    }

}
