package it.polimi.psv.model.cards;

/**
 * One of the possible cards drawn when a player lands on a Dangerous Sector.
 */

public class NoiseInAnySector extends DangerousSectorCard {

    /**
     * Constructor of a Noise in Any Sector card
     *
     * @param hasObject
     *            If true, the player must draw a card from Object Deck.
     */

    public NoiseInAnySector(boolean hasObject) {
        super(hasObject);
    }

}
