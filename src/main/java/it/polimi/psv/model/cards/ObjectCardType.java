package it.polimi.psv.model.cards;

/**
 * Enumeration of all types of Object Card.
 */

public enum ObjectCardType {

    ADRENALINE, ATTACK, DEFENSE, SEDATIVES, SPOTLIGHT, TELEPORT;

}
