package it.polimi.psv.model.cards;

/**
 * One of the possible cards drawn when a player lands on a Dangerous Sector.
 */

public class NoiseInYourSector extends DangerousSectorCard {

    /**
     * Constructor of Noise In Your Sector card.
     *
     * @param hasObject
     *            If true, the player must draw a card from Object Deck.
     */
    public NoiseInYourSector(boolean hasObject) {
        super(hasObject);
    }

}
