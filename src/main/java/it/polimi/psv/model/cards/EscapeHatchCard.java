package it.polimi.psv.model.cards;

import it.polimi.psv.model.sector.EscapeHatchType;

/**
 * Card draw when a Player lands on a Escape Hatch Sector.
 */
public class EscapeHatchCard extends Card {

    private final EscapeHatchType type;

    /**
     * Constructor of the Escape Hatch card.
     *
     * @param type
     *            The type of the card, referring to EscapeHatchSector.
     */

    public EscapeHatchCard(EscapeHatchType type) {
        this.type = type;
    }

    /**
     * Getter of the Escape Hatch Card
     *
     * @return the type of the Escape Hatch Card
     *
     */

    public EscapeHatchType type() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EscapeHatchCard other = (EscapeHatchCard) obj;
        if (type != other.type)
            return false;
        return true;
    }
}
