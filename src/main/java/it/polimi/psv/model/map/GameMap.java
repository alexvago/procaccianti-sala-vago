package it.polimi.psv.model.map;

import it.polimi.psv.model.sector.AlienSector;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.HumanSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.util.HashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * General GameMap class with main map state operations
 * 
 * @author alexvago
 *
 */
public abstract class GameMap {

    private final Map<Integer, Sector> map; // the Map containing the Sectors
    private final HumanSector humanSector; // reference to Human Sector in map
    private final AlienSector alienSector; // reference to Alien Sector in map
    private final List<EscapeHatchSector> escapeHatches;

    public GameMap(Map<Integer, Sector> gameMap, HumanSector humanSector,
            AlienSector alienSector, List<EscapeHatchSector> escapeHatches) {
        this.map = new HashMap<>(gameMap);
        this.humanSector = humanSector;
        this.alienSector = alienSector;
        this.escapeHatches = escapeHatches;
    }

    public Sector getSector(int x, int y) {
        return map.get(HashCode.hashCode(x, y));
    }

    /**
     * Checks if a sector is in the map
     * 
     * @param sector
     *            the sector we are looking for
     * @return
     */
    public boolean isSectorInMap(Sector sector) {
        return map.get(sector.hashCode()) != null;

    }

    /**
     * Gives to the caller a copy of the Map collection
     * 
     * @return a copy of the inner Map
     */
    public Map<Integer, Sector> getMap() {
        return new HashMap<Integer, Sector>(map);
    }

    /**
     * A method for getting all the sectors
     * 
     * @param start
     * @param movement
     * @return
     */
    public abstract Set<Sector> getSectorsInRange(Sector start, int movement);

    public HumanSector getHumanSector() {
        return humanSector;
    }

    public AlienSector getAlienSector() {
        return alienSector;
    }

    public List<EscapeHatchSector> getEscapeHatces() {
        return escapeHatches;
    }

}
