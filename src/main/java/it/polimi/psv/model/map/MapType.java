package it.polimi.psv.model.map;

import java.io.File;
import java.io.Serializable;

/**
 * associates the map name to the XML file location
 * 
 * @author alexvago
 *
 */
public enum MapType implements Serializable {

    GALILEI("src/MapsXML/Galilei.xml"), GALVANI("src/MapsXML/Galvani.xml"), FERMI(
            "src/MapsXML/Fermi.xml");

    private File file;

    private MapType(String s) {

        this.file = new File(s);
    }

    /**
     * return the XML map file
     * 
     * @return XML file with map
     */
    public File getFile() {
        return this.file;
    }
}
