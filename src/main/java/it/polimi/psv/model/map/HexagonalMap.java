package it.polimi.psv.model.map;

import it.polimi.psv.model.sector.AlienSector;
import it.polimi.psv.model.sector.EscapeHatchSector;
import it.polimi.psv.model.sector.HumanSector;
import it.polimi.psv.model.sector.Sector;
import it.polimi.psv.model.sector.VoidSector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HexagonalMap extends GameMap {

    private static final Integer[][] directions = { { +1, 0 }, { +1, -1 },
            { 0, -1 }, { -1, 0 }, { -1, +1 }, { 0, +1 } };

    public HexagonalMap(Map<Integer, Sector> gameMap, HumanSector hs,
            AlienSector as, List<EscapeHatchSector> eh) {
        super(gameMap, hs, as, eh);
    }

    /** just for testing */
    public HexagonalMap(Map<Integer, Sector> gameMap) {
        super(gameMap, null, null, null);
    }

    /**
     * Find the Sectors near to the chosen sector. Returns a list.
     *
     * @param sector
     *            The sector whose I want to find the neighbors.
     * @return ArrayList containing the adjacent sectors
     */
    private Set<Sector> getNeighbors(Sector sector) {
        Set<Sector> set = new HashSet<>();
        for (int i = 0; i < 6; i++) {
            Sector s = getSector(sector.getX() + directions[i][0],
                    sector.getY() + directions[i][1]);

            if (isAcceptableSector(s)) {
                set.add(s);
            }
        }
        return set;
    }

    /**
     * private method useful in getSectorsInRange and getNeighbors
     * 
     * @param sector
     *            starting sector
     * @param direction
     *            two ints that must be added to sector coordinates
     * @return the neighbor sector in the given direction
     */
    private Sector getNeighbor(Sector sector, Integer[] direction) {
        return getSector(sector.getX() + direction[0], sector.getY()
                + direction[1]);

    }

    /**
     * Method for finding all the sectors in a given range.
     * 
     * @param start
     *            Starting sector
     * @param movement
     *            the maximum distance reachable
     * @return visited a Set of sectors reachable with the given speed movement
     */
    @Override
    public Set<Sector> getSectorsInRange(Sector start, int movement) {
        if (movement == 1) {
            return getNeighbors(start);
        }
        Set<Sector> visited = new HashSet<>();

        List<ArrayList<Sector>> fringes = new ArrayList<>();
        fringes.add(new ArrayList<Sector>());
        fringes.get(0).add(start);

        for (int i = 1; i <= movement; i++) {
            fringes.add(new ArrayList<Sector>());
            for (Sector s : fringes.get(i - 1)) {
                for (Integer[] dir : directions) {
                    Sector neighbor = getNeighbor(s, dir);
                    if (isAcceptableSector(neighbor)
                            && !visited.contains(neighbor)) {
                        visited.add(neighbor);
                        fringes.get(i).add(neighbor);
                    }
                }
            }
        }
        visited.remove(start);
        return visited;
    }

    /**
     * Check if the sector given is a possible movement option
     * 
     * @param sector
     * @return true if movement in that sector is possible
     */
    private boolean isAcceptableSector(Sector sector) {
        return ((sector instanceof Sector) && !(sector instanceof VoidSector)
                && !(sector instanceof HumanSector) && !(sector instanceof AlienSector));
    }
}
