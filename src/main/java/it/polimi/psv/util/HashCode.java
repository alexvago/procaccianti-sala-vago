package it.polimi.psv.util;

public class HashCode {

    private HashCode() {
    }

    public static int hashCode(int x, int y) {

        final int prime = 31;
        int result = 2;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }
}
