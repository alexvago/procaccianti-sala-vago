package it.polimi.psv.client;

import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Logic of a subscriber, who receive the messages sent from the server.
 */
public class Subscriber implements Runnable {

    private Scanner input;
    private Logger LOGGER;

    public Subscriber(Socket subscriberSocket, Logger LOGGER) {
        this.LOGGER = LOGGER;
        try {
            this.input = new Scanner(subscriberSocket.getInputStream());
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            LOGGER.print(input.nextLine());
        }

    }

}
