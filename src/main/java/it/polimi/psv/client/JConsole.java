package it.polimi.psv.client;

import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class JConsole extends JTextArea {

    /**
     * 
     */
    private static final long serialVersionUID = -3111332201616001045L;

    public JConsole(String string) {
        super(string);
        DefaultCaret caret = (DefaultCaret) this.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    public void append(String text) {
        super.append(text);
        this.setCaretPosition(this.getCaretPosition() + text.length());
    }

}
