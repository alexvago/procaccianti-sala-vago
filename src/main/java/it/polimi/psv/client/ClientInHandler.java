package it.polimi.psv.client;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Manage how a client obtain informations from server.
 * 
 * @author alexvago
 */
public class ClientInHandler implements Runnable {

    private final ObjectInputStream stream;

    public ClientInHandler(ObjectInputStream stream) throws IOException {
        this.stream = stream;
    }

    /**
     * Read the object sent from server.
     */
    @Override
    public void run() {

    }

    public Object readObject() throws ClassNotFoundException, IOException {
        return stream.readObject();
    }

}
