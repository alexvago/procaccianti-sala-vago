package it.polimi.psv.client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class HexagonComponent extends JLabel {

    private Hexagon hexagon;
    private Point center;
    private int color;
    private String text;
    private Icon icon;

    private static final long serialVersionUID = 4552953075128682820L;

    public HexagonComponent(Point center, int radius) {
        this.hexagon = new Hexagon(center, radius);
        this.center = center;
        this.setVisible(true);
    }

    public HexagonComponent(int x, int y, int radius, String text, Icon icon) {
        this.text = text;
        this.icon = icon;
        this.setIcon(icon);
        this.hexagon = new Hexagon(x, y, radius);
        this.center = new Point(x, y);
        this.setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        draw(g, new Color(color));
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Polygon getPolygon() {
        return hexagon;
    }

    public void draw(Graphics g, Color c) {
        hexagon.draw((Graphics2D) g, 1, c.getRGB(), false);
        icon.paintIcon(this, g, center.x - hexagon.getRadius(),
                (int) (center.y - (Math.sqrt(3) / 2 * hexagon.getRadius())));
    }

    @Override
    public boolean contains(Point p) {
        return hexagon.contains(p);
    }

    public void assignIcon(ImageIcon icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return text;
    }
}
