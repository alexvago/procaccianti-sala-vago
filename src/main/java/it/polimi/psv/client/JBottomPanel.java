package it.polimi.psv.client;

import it.polimi.psv.controller.useractions.UserAction;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

public class JBottomPanel extends JPanel {

    private static final long serialVersionUID = -3071600565577378449L;

    private JButton moveButton = new JButton("Move");
    private JButton attackButton = new JButton("Attack");
    private JButton drawCardButton = new JButton("Draw Sector Card");
    private JButton endTurnButton = new JButton("End turn");
    private JButton useCardButton = new JButton("Use Card");
    private JButton skipMoveButton = new JButton("Skip movement");
    private JButton revealIdentityButton = new JButton("Reveal user identity");

    private List<JButton> buttons;

    private ClientOutHandler output;

    /**
     * private class ButtonListener for adding custom actions to buttons
     * 
     * @author alexvago
     *
     */
    private class ButtonListener implements ActionListener {

        private UserAction action;

        public ButtonListener(UserAction action) {
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                output.sendObject(action);
                deactivateAll();
            } catch (IOException e1) {
            }

        }
    }

    /**
     * It's the frame's bottom panel which contains the jbuttons that activates
     * all the turn's actions, like attack, end turn, use card.
     * 
     * @param output
     */
    public JBottomPanel(final ClientOutHandler output) {
        this.setLayout(new FlowLayout());
        this.setBackground(Color.BLACK);
        this.output = output;
        this.buttons = new ArrayList<JButton>();

        buttons.add(moveButton);
        moveButton.addActionListener(new ButtonListener(UserAction.MOVE));
        buttons.add(attackButton);
        attackButton.addActionListener(new ButtonListener(UserAction.ATTACK));
        buttons.add(drawCardButton);
        drawCardButton.addActionListener(new ButtonListener(
                UserAction.DRAW_CARD));
        buttons.add(endTurnButton);
        endTurnButton.addActionListener(new ButtonListener(UserAction.ENDTURN));
        buttons.add(useCardButton);
        useCardButton
                .addActionListener(new ButtonListener(UserAction.USE_CARD));
        buttons.add(skipMoveButton);
        skipMoveButton.addActionListener(new ButtonListener(
                UserAction.DO_NOT_MOVE));
        buttons.add(revealIdentityButton);
        revealIdentityButton.addActionListener(new ButtonListener(
                UserAction.REVEAL_IDENTITY));
        deactivateAll();
        for (JButton btn : buttons) {
            btn.setBackground(Color.BLACK);
            btn.setForeground(Color.WHITE);
            this.add(btn);
        }
    }

    public void activateMove() {
        moveButton.setEnabled(true);
    }

    public void activateAttack() {
        attackButton.setEnabled(true);
    }

    public void activateDrawCard() {
        drawCardButton.setEnabled(true);
    }

    public void activateEndTurn() {
        endTurnButton.setEnabled(true);
    }

    public void activateUseCard() {
        useCardButton.setEnabled(true);
    }

    public void activateSkipMove() {
        skipMoveButton.setEnabled(true);
    }

    public void activateRevealIdentity() {
        revealIdentityButton.setEnabled(true);
    }

    public void deactivateMove() {
        moveButton.setEnabled(false);
    }

    public void deactivateAttack() {
        attackButton.setEnabled(false);
    }

    public void deactivateDrawCard() {
        drawCardButton.setEnabled(false);
    }

    public void deactivateEndTurn() {
        endTurnButton.setEnabled(false);
    }

    public void deactivateUseCard() {
        useCardButton.setEnabled(false);
    }

    public void deactivateSkipMove() {
        skipMoveButton.setEnabled(false);
    }

    public void deactivateRevealIdentity() {
        revealIdentityButton.setEnabled(false);
    }

    public void deactivateAll() {
        deactivateMove();
        deactivateAttack();
        deactivateDrawCard();
        deactivateEndTurn();
        deactivateUseCard();
        deactivateSkipMove();
        deactivateRevealIdentity();
    }

    public void setOutput(ClientOutHandler output) {
        this.output = output;
    }
}
