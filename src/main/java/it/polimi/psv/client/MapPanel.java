package it.polimi.psv.client;

import it.polimi.psv.model.map.MapType;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class MapPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int WIDTH = 1400;
    private static final int HEIGHT = 700;
    private MapType map;

    private HexagonComponent[] hexagons = new HexagonComponent[322];

    private Font font = new Font("Courier", Font.PLAIN, 12);
    FontMetrics metrics;

    public MapPanel(MapType map) {
        this.map = map;
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        Point origin = new Point(40, 30);

        g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE,
                BasicStroke.JOIN_MITER));
        g2d.setFont(font);
        metrics = g.getFontMetrics();

        drawHexGridLoop(g2d, origin, 25);
    }

    private void drawHexGridLoop(Graphics g, Point origin, int radius) {

        double apotema = Math.sqrt(3) / 2 * radius;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document parsedMap = builder.parse(map.getFile());

            NodeList columnsList = parsedMap.getElementsByTagName("column");

            for (int i = 0; i < columnsList.getLength(); i++) {

                Element column = (Element) columnsList.item(i);

                NodeList sectorsList = column.getElementsByTagName("sector");
                for (int j = 0; j < sectorsList.getLength(); j++) {
                    Element sectorElement = (Element) sectorsList.item(j);
                    String sectorType = sectorElement
                            .getElementsByTagName("type").item(0)
                            .getTextContent();

                    int xC = origin.x + (i * (((3) * radius) / 2));
                    int yC = (int) (origin.y + ((i % 2) * apotema) + (2 * j * apotema));

                    drawHex(g, j, i, xC, yC, radius, sectorType);

                }
            }
        } catch (Exception e) {

        }
    }

    private void drawHex(Graphics g, int j, int i, int x, int y, int r,
            String sectorType) {
        Graphics2D g2d = (Graphics2D) g;

        String text = new String((char) (i + 65) + " "
                + String.format("%02d", j + 1));
        HexagonComponent hex;
        Icon icon;
        int color = this.getBackground().getRGB();
        Color textColor = Color.BLACK;

        boolean textVisible;
        switch (sectorType) {
        case "SecureSector":
            icon = new ImageIcon("src/assets/secure.png");
            hex = new HexagonComponent(x, y, r, text, icon);
            textVisible = true;
            break;
        case "DangerousSector":
            icon = new ImageIcon("src/assets/dangerous.png");
            hex = new HexagonComponent(x, y, r, text, icon);
            textVisible = true;
            break;
        case "AlienSector":
            icon = new ImageIcon("src/assets/alienSector.png");
            hex = new HexagonComponent(x, y, r, text, icon);
            textVisible = false;
            break;
        case "HumanSector":
            icon = new ImageIcon("src/assets/humanSector.png");
            hex = new HexagonComponent(x, y, r, text, icon);
            textVisible = false;
            break;
        case "EscapeHatchSector":
            icon = new ImageIcon("src/assets/escapeBlack.png");
            hex = new HexagonComponent(x, y, r, text, icon);
            textVisible = true;
            textColor = Color.WHITE;
            break;
        default:
            hex = new HexagonComponent(x, y, r, text, new ImageIcon());
            textVisible = false;
            break;
        }

        this.add(hex);

        hexagons[j + 14 * i] = hex;

        hex.draw(g2d, new Color(color));
        g.setColor(textColor);
        if (textVisible)
            g.drawString(text.replaceFirst("\\s", ""), x - 11, y + 4);

    }

    public HexagonComponent[] getHexagons() {
        return hexagons;
    }

}