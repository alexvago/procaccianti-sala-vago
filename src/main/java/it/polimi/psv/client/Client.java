package it.polimi.psv.client;

import it.polimi.psv.controller.events.AskForCardToUseEvent;
import it.polimi.psv.controller.events.AskForMapEvent;
import it.polimi.psv.controller.events.AskForPositionEvent;
import it.polimi.psv.controller.events.AskForRevealNameEvent;
import it.polimi.psv.controller.events.AskForRulesEvent;
import it.polimi.psv.controller.events.AskToDiscardEvent;
import it.polimi.psv.controller.events.EndGameEvent;
import it.polimi.psv.controller.events.Event;
import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NoiseInYourSectorEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.events.SendPossibleActionsEvent;
import it.polimi.psv.controller.events.SilenceEvent;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.useractions.UserAction;
import it.polimi.psv.model.cards.ObjectCardType;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The client application, manage the communication between client and server
 * and the client creation.
 * 
 * @author alexvago
 */
public class Client {

    /**
     * Logger starts as CLI
     */
    private static Logger LOGGER;

    /**
     * the IP address of the server
     */
    private static final String IP = "127.0.0.1";

    // is the port that the Server uses to wait connections
    private static final int PORT = 29999;

    // port for pub-sub
    private static final int SUBSCRIBER_PORT = 29998;

    // the socket for connection
    private Socket socket;

    private Socket subscriberSocket;

    private ClientInHandler input;
    private ClientOutHandler output;

    private List<UserAction> possibleActions;

    /**
     * Client username
     */
    private final String username;

    private boolean gameEnd = false;

    public Client(String username, Logger logger) throws IOException {
        this.username = username;
        LOGGER = logger;
    }

    /**
     * Tries to close socket
     */
    public void close() {
        try {
            socket.close();
            subscriberSocket.close();
        } catch (IOException e) {
            LOGGER.print("Can't close socket connection!");
            LOGGER.print(e);
        }
    }

    /**
     * starts the Client
     * 
     * @throws IOException
     */
    public void startClient() throws IOException {

        /**
         * creates a new socket that refers to the machine with the specified IP
         * and PORT 127.0.0.1 refers to your local machine, change this address
         * if you want to use your program in the network (in case of problems
         * check your firewall preferences)
         */
        socket = new Socket(IP, PORT);
        subscriberSocket = new Socket(IP, SUBSCRIBER_PORT);

        ObjectOutputStream outputStream = new ObjectOutputStream(
                socket.getOutputStream());
        ObjectInputStream inputStream = new ObjectInputStream(
                socket.getInputStream());

        input = new ClientInHandler(inputStream);
        output = new ClientOutHandler(outputStream, LOGGER);

        Subscriber subscriber = new Subscriber(subscriberSocket, LOGGER);

        LOGGER.print("Connection Established.");

        ExecutorService executor = Executors.newFixedThreadPool(3);

        executor.submit(input);
        executor.submit(output);
        executor.submit(subscriber);

        output.print(username);
        Scanner scanner = new Scanner(System.in);
        try {
            while (!gameEnd) {
                waitForEvent(scanner);
            }
            executor.shutdown();
            close();
        } catch (ClassNotFoundException | IOException e) {
            LOGGER.print("Problems with server communication. Closing connection..");
            LOGGER.print(e);
            close();
        }
    }

    /**
     * Wait for an event send from the server. If the event type is known, then
     * execute the actions under the if clauses, otherwise send an error
     * message, or ask the user to reinsert the input.
     * 
     * @param scanner
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void waitForEvent(Scanner scanner) throws ClassNotFoundException,
            IOException {

        Event event = (Event) input.readObject();

        if (event instanceof MessageEvent) {
            LOGGER.print(((MessageEvent) event).getMessage());
        }

        /**
         * ask user to choose an action
         */
        if (event instanceof SendPossibleActionsEvent) {
            possibleActions = ((SendPossibleActionsEvent) event)
                    .getPossibleActions();
            int choice;
            do {
                LOGGER.print("Possible actions:");
                for (int i = 0; i < possibleActions.size(); i++) {
                    LOGGER.print("[" + i + "] " + possibleActions.get(i));
                }
                try {
                    choice = scanner.nextInt();

                } catch (InputMismatchException e) {
                    LOGGER.print("Input must be the number of the option choosen!");
                    choice = -1;
                }
                scanner.nextLine();
            } while (choice >= possibleActions.size() || choice < 0);
            output.sendObject(possibleActions.get(choice));
        }

        if (event instanceof AskForPositionEvent) {

            String input = null;
            do {
                LOGGER.print(((AskForPositionEvent) event).getMessage());
                input = scanner.nextLine();

            } while (!input.matches("[a-wA-W]\\s\\b([1-9]|1[0-4])\\b"));

            StringTokenizer tokenizer = new StringTokenizer(input);
            int x = ((int) tokenizer.nextToken().toUpperCase().charAt(0)) - 65;
            int y = Integer.valueOf(tokenizer.nextToken()) - x / 2 - 1;

            output.sendObject(new int[] { x, y });
        }

        if (event instanceof SilenceEvent) {
            LOGGER.print(((SilenceEvent) event).getMessage());
            output.sendObject(scanner.nextLine());
        }
        if (event instanceof NoiseInYourSectorEvent) {
            LOGGER.print(((NoiseInYourSectorEvent) event).getMessage());
            output.sendObject(scanner.nextLine());
        }

        /**
         * ask the user to choose a card to play
         */
        if (event instanceof AskForCardToUseEvent) {
            LOGGER.print("You can use one of these objects:");
            ObjectCardType[] types = ((AskForCardToUseEvent) event).getTypes();

            printChoices(types, scanner);
        }

        /**
         * ask user to choose a card to discard
         */
        if (event instanceof AskToDiscardEvent) {
            LOGGER.print("You have too many cards, you must discard one!");
            ObjectCardType[] types = ((AskToDiscardEvent) event).getTypes();

            printChoices(types, scanner);
        }

        /**
         * Ask user to choose rules type
         */
        if (event instanceof AskForRulesEvent) {
            RuleType[] types = ((AskForRulesEvent) event).getRules();
            int choice;
            do {
                LOGGER.print("Choose rules type:");
                for (int i = 0; i < types.length; i++) {
                    LOGGER.print("[" + i + "] " + types[i].name());
                }
                choice = scanner.nextInt();
                scanner.nextLine();
            } while (choice >= types.length || choice < 0);

            output.sendObject(choice);
        }
        /**
         * Ask user to choose a map
         */
        if (event instanceof AskForMapEvent) {
            MapType[] maps = ((AskForMapEvent) event).getMaps();
            int choice;
            do {
                LOGGER.print("Choose map:");
                for (int i = 0; i < maps.length; i++) {
                    LOGGER.print("[" + i + "] " + maps[i].name());
                }
                choice = scanner.nextInt();
                scanner.nextLine();
            } while (choice >= maps.length || choice < 0);

            output.sendObject(choice);
        }

        /**
         * notify game start to users
         */
        if (event instanceof NotifyGameStartEvent) {
            LOGGER.print(((NotifyGameStartEvent) event).getMessage());
        }

        if (event instanceof EndGameEvent) {
            LOGGER.print("Game ended!");
            gameEnd = true;
        }

        if (event instanceof AskForRevealNameEvent) {
            LOGGER.print("Choose the player of which you want to discover the identity:");
            String[] names = ((AskForRevealNameEvent) event).getNames();
            int choice;
            do {
                for (int i = 0; i < names.length; i++) {
                    LOGGER.print("[" + i + "] " + names[i]);
                }

                try {
                    choice = scanner.nextInt();

                } catch (InputMismatchException e) {
                    LOGGER.print("Input must be the number of the option choosen!");
                    choice = -1;
                }
                scanner.nextLine();
            } while (choice >= names.length || choice < 0);
            output.sendObject(names[choice]);
        }

    }

    /**
     * Print the possible choices to Users' CLI when a Human decide to
     * use/discard a card.
     * 
     * @param types
     *            Array containing the type of the user's Object Cards.
     * @param scanner
     * @throws IOException
     */

    private void printChoices(ObjectCardType[] types, Scanner scanner)
            throws IOException {
        int choice;
        do {
            for (int i = 0; i < types.length; i++) {
                LOGGER.print("[" + i + "] " + types[i]);
            }

            try {
                choice = scanner.nextInt();

            } catch (InputMismatchException e) {
                LOGGER.print("Input must be the number of the option choosen!");
                choice = -1;
            }
            scanner.nextLine();
        } while (choice >= types.length || choice < 0);
        output.sendObject(types[choice]);
    }

}
