package it.polimi.psv.client;

import it.polimi.psv.controller.events.AskForCardToUseEvent;
import it.polimi.psv.controller.events.AskForMapEvent;
import it.polimi.psv.controller.events.AskForPositionEvent;
import it.polimi.psv.controller.events.AskForRevealNameEvent;
import it.polimi.psv.controller.events.AskForRulesEvent;
import it.polimi.psv.controller.events.AskToDiscardEvent;
import it.polimi.psv.controller.events.EndGameEvent;
import it.polimi.psv.controller.events.Event;
import it.polimi.psv.controller.events.MessageEvent;
import it.polimi.psv.controller.events.NoiseInYourSectorEvent;
import it.polimi.psv.controller.events.NotifyGameStartEvent;
import it.polimi.psv.controller.events.SendPossibleActionsEvent;
import it.polimi.psv.controller.events.SilenceEvent;
import it.polimi.psv.controller.game.RuleType;
import it.polimi.psv.controller.useractions.UserAction;
import it.polimi.psv.model.map.MapType;
import it.polimi.psv.view.logger.GUILogger;
import it.polimi.psv.view.logger.Logger;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class GUIClient {

    /**
     * Logger starts as CLI
     */
    private static Logger LOGGER;
    private static Logger subLogger;

    /**
     * the IP address of the server
     */
    private static final String IP = "127.0.0.1";

    // is the port that the Server uses to wait connections
    private static final int PORT = 29999;

    // port for pub-sub
    private static final int SUBSCRIBER_PORT = 29998;

    // the socket for connection
    private Socket socket;

    private Socket subscriberSocket;

    private ClientInHandler input;
    private ClientOutHandler output;
    /**
     * Client username
     */
    private String username;

    private boolean gameEnd = false;

    final JTextArea console = new JConsole("Server communications\n");
    final JTextArea subscriberConsole = new JConsole("Game messages\n");
    JBottomPanel bottomPanel;
    MapPanel leftPanel;
    JPanel mainPanel = new JPanel(new BorderLayout());
    JPanel logoPanel = new JPanel();

    public GUIClient() {
        this.username = JOptionPane.showInputDialog("Your name");
        if (this.username != null) {
            LOGGER = new GUILogger(console);
            subLogger = new GUILogger(subscriberConsole);
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    Main();

                }
            });
        } else
            System.exit(0);
    }

    /**
     * Method that manage the logic of a client, remains active until the User
     * closes the client.
     * 
     * @throws IOException
     */
    public void startClient() throws IOException {

        /**
         * creates a new socket that refers to the machine with the specified IP
         * and PORT 127.0.0.1 refers to your local machine, change this address
         * if you want to use your program in the network (in case of problems
         * check your firewall preferences)
         */
        socket = new Socket(IP, PORT);
        subscriberSocket = new Socket(IP, SUBSCRIBER_PORT);

        ObjectOutputStream outputStream = new ObjectOutputStream(
                socket.getOutputStream());
        ObjectInputStream inputStream = new ObjectInputStream(
                socket.getInputStream());

        input = new ClientInHandler(inputStream);
        output = new ClientOutHandler(outputStream, LOGGER);

        bottomPanel = new JBottomPanel(output);
        mainPanel.add(bottomPanel, BorderLayout.SOUTH);
        mainPanel.repaint();
        mainPanel.validate();

        Subscriber subscriber = new Subscriber(subscriberSocket, subLogger);

        LOGGER.print("Connection Established.");

        ExecutorService executor = Executors.newFixedThreadPool(3);

        executor.submit(input);
        executor.submit(output);
        executor.submit(subscriber);

        output.print(username);
        try {
            while (!gameEnd) {
                waitForEvent();
            }
            executor.shutdown();
            close();
        } catch (ClassNotFoundException | IOException e) {
            LOGGER.print("Problems with server communication. Closing connection..");
            LOGGER.print(e);
            close();
        }
    }

    /**
     * Tries to close socket
     */
    public void close() {
        try {
            socket.close();
            subscriberSocket.close();
        } catch (IOException e) {
            LOGGER.print("Can't close socket connection!");
            LOGGER.print(e);
        }
    }

    public void Main() {

        int width = 1300;
        int height = 700;

        JFrame frame = new JFrame("Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(width, height);
        frame.setResizable(false);

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.setSize(250, height);
        rightPanel.setBackground(Color.BLACK);

        console.setColumns(36);
        console.setEditable(false);
        console.setBackground(Color.BLACK);
        console.setForeground(Color.WHITE);
        console.setAutoscrolls(true);

        subscriberConsole.setEditable(false);
        subscriberConsole.setBackground(Color.BLACK);
        subscriberConsole.setForeground(Color.WHITE);
        subscriberConsole.setAutoscrolls(true);

        JScrollPane consoleScroll = new JScrollPane(console);
        consoleScroll.setAutoscrolls(true);

        JScrollPane subscriberScroll = new JScrollPane(subscriberConsole);

        JTextArea userNotes = new JTextArea("use this area for notes");
        userNotes.setColumns(36);
        userNotes.setRows(10);

        rightPanel.add(consoleScroll);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        rightPanel.add(subscriberScroll);
        rightPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        rightPanel.add(new JScrollPane(userNotes));

        try {
            logoPanel.add(new JLabel(new ImageIcon(ImageIO.read(
                    new File("src/assets/background.jpg")).getScaledInstance(
                    900, 700, BufferedImage.SCALE_DEFAULT))));
        } catch (IOException e) {
            LOGGER.print("cannot get background image");
        }

        logoPanel.setBackground(Color.BLACK);
        mainPanel.add(logoPanel, BorderLayout.CENTER);

        mainPanel.add(rightPanel, BorderLayout.EAST);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);

    }

    public JTextArea getTextArea() {
        return console;
    }

    /**
     * Wait for an event send from the server. If the event type is known, then
     * execute the actions under the if clauses, otherwise send an error
     * message, or ask the user to reinsert the input.
     * 
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void waitForEvent() throws ClassNotFoundException, IOException {

        Event event = (Event) input.readObject();

        if (event instanceof MessageEvent) {
            String message = ((MessageEvent) event).getMessage();
            LOGGER.print(message);
        }

        /**
         * ask user to choose an action
         */
        if (event instanceof SendPossibleActionsEvent) {
            List<UserAction> actions = ((SendPossibleActionsEvent) event)
                    .getPossibleActions();

            for (UserAction action : actions) {
                switch (action) {
                case MOVE:

                    bottomPanel.activateMove();
                    break;
                case ATTACK:
                    bottomPanel.activateAttack();
                    break;
                case DRAW_CARD:
                    bottomPanel.activateDrawCard();
                    break;
                case ENDTURN:
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            bottomPanel.activateEndTurn();
                        }
                    });
                    break;
                case USE_CARD:
                    bottomPanel.activateUseCard();
                    break;
                case DO_NOT_MOVE:
                    bottomPanel.activateSkipMove();
                    break;
                case REVEAL_IDENTITY:
                    bottomPanel.activateRevealIdentity();
                    break;

                default:
                    break;
                }
            }
            mainPanel.revalidate();
            mainPanel.repaint();

        }

        if (event instanceof AskForPositionEvent) {

            // JDialog asking for coordinates or click on sector(needs clickable
            // map)
            String message = ((AskForPositionEvent) event).getMessage();
            LOGGER.print(message);

            leftPanel.addMouseListener(new MouseListener() {

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseExited(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    Point p = e.getPoint();
                    HexagonComponent[] hexagons = leftPanel.getHexagons();
                    for (int j = 0; j < hexagons.length; j++)
                        if (hexagons[j].contains(p)) {
                            hexagons[j].setColor(0xFF00FF);
                            hexagons[j].repaint();
                            hexagons[j].validate();
                            StringTokenizer tokenizer = new StringTokenizer(
                                    hexagons[j].toString());
                            int x = ((int) tokenizer.nextToken().toUpperCase()
                                    .charAt(0)) - 65;
                            int y = Integer.valueOf(tokenizer.nextToken()) - x
                                    / 2 - 1;

                            try {
                                output.sendObject(new int[] { x, y });
                            } catch (IOException e1) {
                                LOGGER.print("cannot send position to server");
                            }
                            leftPanel.removeMouseListener(this);
                            break;
                        }

                }
            });
        }

        if (event instanceof SilenceEvent) {
            String message = ((SilenceEvent) event).getMessage();

            String[] options = new String[] { "OK" };
            JOptionPane.showOptionDialog(null, message, "Silence!",
                    JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE,
                    null, options, 0);
            output.sendObject("");
        }
        if (event instanceof NoiseInYourSectorEvent) {
            String message = ((NoiseInYourSectorEvent) event).getMessage();

            String[] options = new String[] { "OK" };
            JOptionPane.showOptionDialog(null, message,
                    "Noise in your sector!", JOptionPane.OK_OPTION,
                    JOptionPane.INFORMATION_MESSAGE, null, options, 0);
            output.sendObject("");
        }

        /**
         * ask the user to choose a card to play
         */
        if (event instanceof AskForCardToUseEvent) {
            Object[] cards = ((AskForCardToUseEvent) event).getTypes();
            int choice;
            do {
                choice = JOptionPane.showOptionDialog(null,
                        "Choose a card to use", "Card selection",
                        JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE,
                        null, cards, 0);
            } while (choice == JOptionPane.CLOSED_OPTION);

            output.sendObject(cards[choice]);
        }

        /**
         * ask user to choose a card to discard
         */
        if (event instanceof AskToDiscardEvent) {
            Object[] cards = ((AskToDiscardEvent) event).getTypes();
            int choice;
            do {
                choice = JOptionPane.showOptionDialog(null,
                        "Choose a card to discard",
                        "Card to discard selection", JOptionPane.OK_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null, cards, 0);
            } while (choice == JOptionPane.CLOSED_OPTION);

            output.sendObject(cards[choice]);
        }

        /**
         * Ask user to choose rules type
         */
        if (event instanceof AskForRulesEvent) {
            RuleType[] types = ((AskForRulesEvent) event).getRules();
            int choice;
            do {
                choice = JOptionPane.showOptionDialog(null, "Choose rules",
                        "Rules selection", JOptionPane.OK_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null, types, 0);
            } while (choice == JOptionPane.CLOSED_OPTION);

            output.sendObject(choice);
        }
        /**
         * Ask user to choose a map
         */
        if (event instanceof AskForMapEvent) {
            MapType[] maps = ((AskForMapEvent) event).getMaps();
            int choice;
            do {
                choice = JOptionPane.showOptionDialog(null, "Choose map",
                        "Map selection", JOptionPane.OK_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null, maps, 0);
            } while (choice == JOptionPane.CLOSED_OPTION);

            output.sendObject(choice);
        }

        if (event instanceof EndGameEvent) {
            // notify game end, maybe ask for new game(which means reconnection
            // to server)
            LOGGER.print("Game ended!");
            gameEnd = true;
        }

        if (event instanceof NotifyGameStartEvent) {
            MapType map = ((NotifyGameStartEvent) event).getMap();
            RuleType rules = ((NotifyGameStartEvent) event).getRules();
            leftPanel = new MapPanel(map);

            mainPanel.remove(logoPanel);

            mainPanel.add(leftPanel, BorderLayout.CENTER);
            mainPanel.revalidate();
            mainPanel.repaint();

            LOGGER.print("Starting game on map " + map + " with rules " + rules);

        }

        if (event instanceof AskForRevealNameEvent) {
            String[] names = ((AskForRevealNameEvent) event).getNames();
            int choice;
            do {
                choice = JOptionPane
                        .showOptionDialog(
                                null,
                                "Choose the player of which you want to discover the identity",
                                "Player selection", JOptionPane.OK_OPTION,
                                JOptionPane.INFORMATION_MESSAGE, null, names, 0);
            } while (choice >= names.length || choice < 0);
            output.sendObject(names[choice]);
        }

    }
}
