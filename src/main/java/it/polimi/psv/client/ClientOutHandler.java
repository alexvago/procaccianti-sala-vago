package it.polimi.psv.client;

import it.polimi.psv.view.logger.Logger;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Manage how the client send informations to the server.
 * 
 * @author alexvago
 */
public class ClientOutHandler implements Runnable {

    private final ObjectOutputStream stream;
    private final Logger LOGGER;

    public ClientOutHandler(ObjectOutputStream stream, Logger lOGGER)
            throws IOException {
        this.stream = stream;
        this.LOGGER = lOGGER;
    }

    @Override
    public void run() {

    }

    /**
     * send the object to the server. Throws IOException
     * 
     * @param arg
     *            the string sent to server.
     */
    public void sendObject(Object arg) throws IOException {
        stream.writeObject(arg);
        stream.flush();
    }

    /**
     * Print the message sent to server.
     * 
     * @param message
     *            The string printed.
     */
    public void print(String message) {
        try {
            stream.writeObject(message);
            stream.flush();
            LOGGER.print("Sent " + message);
        } catch (IOException e) {
            LOGGER.print("Cannot write message to server!");
            LOGGER.print(e);
        }

    }

}
